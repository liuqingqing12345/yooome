package factory

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"yooome/core/container"
	validInterface "yooome/core/valid_interface"
)

// 表单参数验证器工厂
func ValidFormFactory(key string) func(context *gin.Context) {
	if value := container.NewContainersFactory().Get(key); value != nil {
		if val, isOk := value.(validInterface.ValidatorInterface); isOk{
			return val.ValidatorParams
		}
	}
	fmt.Println("表单验证器验证错误....")
	return nil
}