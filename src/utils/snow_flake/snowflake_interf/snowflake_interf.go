package snowflake_interf
type SnowFlakeInterface interface {
	GetID() int64
}