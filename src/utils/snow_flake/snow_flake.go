package snow_flake

import (
	"sync"
	"time"
	"yooome/global/contsts"
	"yooome/global/variable"
	"yooome/utils/snow_flake/snowflake_interf"
)

type snowflake struct {
	sync.Mutex
	timestamp int64
	machineId int64
	sequence  int64
}

// 创建一个雪花算法生成器
func NewSnowFlakeFactory() snowflake_interf.SnowFlakeInterface {
	return &snowflake{
		timestamp: 0,
		machineId: variable.ConfigYml.GetInt64("SnowFlake.SnowFlakeMachineId"),
		sequence:  0,
	}
}

func (s *snowflake) GetID() int64 {
	s.Lock()
	defer func() {
		s.Unlock()
	}()
	now := time.Now().UnixNano() / 1e6
	if s.timestamp == now {
		s.sequence = (s.sequence + 1) & contsts.SequenceMask
		if s.sequence == 0 {
			for now <= s.timestamp {
				now = time.Now().UnixNano() / 1e6
			}
		}
	} else {
		s.sequence = 0
	}
	s.timestamp = now
	r := (now-contsts.StartTimeStamp) << contsts.TimestampShift | (s.machineId << contsts.MachineIdShift) | (s.sequence)
	return r
}
