package isempty

import "reflect"

func IsNil(value interface{}, traceSource ...bool) bool {
	if value == nil {
		return true
	}
	var val reflect.Value
	if v, ok := value.(reflect.Value); ok {
		val = v
	} else {
		val = reflect.ValueOf(value)
	}
	switch val.Kind() {
	case reflect.Chan,
		reflect.Slice,
		reflect.Map,
		reflect.Func,
		reflect.Interface,
		reflect.UnsafePointer:
		return !val.IsValid() || val.IsNil()
	case reflect.Ptr:
		if len(traceSource) > 0 && traceSource[0] {
			for val.Kind() == reflect.Ptr {
				val = val.Elem()
			}
			if !val.IsValid() {
				return true
			}
			if val.Kind() == reflect.Ptr {
				return val.IsNil()
			}
		} else {
			return !val.IsValid() || val.IsNil()
		}
	}
	return false
}
