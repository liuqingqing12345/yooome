package convert

func StrToSliceStr(i interface{}) []string {
	return InterfaceToSliceStr(i)
}

// []interface{} convert []string
func InterfaceToSliceStr(i interface{}) []string {
	if i == nil {
		return nil
	}
	var array []string
	switch value := i.(type) {
	case []int:
		array = make([]string, len(value))
		for k, v := range value {
			array[k] = String(v)
		}
	case []int8:
		array = make([]string, len(value))
		for k, v := range value {
			array[k] = String(v)
		}
	case []int16:
		array = make([]string, len(value))
		for k, v := range value {
			array[k] = String(v)
		}
	case []int32:
		array = make([]string, len(value))
		for k, v := range value {
			array[k] = String(v)
		}
	case []int64:
		array = make([]string, len(value))
		for k, v := range value {
			array[k] = String(v)
		}
	case []uint:
		array = make([]string, len(value))
		for k, v := range value {
			array[k] = String(v)
		}
	case []uint16:
		array = make([]string, len(value))
		for idx, val := range value {
			array[idx] = String(val)
		}
	case []uint32:
		array = make([]string, len(value))
		for idx, val := range value {
			array[idx] = String(val)
		}
	case []uint64:
		array = make([]string, len(value))
		for idx, val := range value {
			array[idx] = String(val)
		}
	case []bool:
		array = make([]string, len(value))
		for idx, val := range value {
			array[idx] = String(val)
		}
	case []float32:
		array = make([]string, len(value))
		for idx, val := range value {
			array[idx] = String(val)
		}
	case []float64:
		array = make([]string, len(value))
		for idx, val := range value {
			array[idx] = String(val)
		}
	case []interface{}:
		array = make([]string, len(value))
		for idx, val := range value {
			array[idx] = String(val)
		}
	case []string:
		array = value
	case [][]byte:
		array = make([]string,len(value))
		for idx,val := range value{
			array[idx] = String(val)
		}
	default:
		return []string{String(i)}
		
	}
	return array
}
