package ytime

import "time"

type wrapper struct {
	time.Time
}

func (w *wrapper) String() string {
	if w.IsZero() {
		return ""
	}
	return w.Format("2006-01-02 15:04:05")
}
