package yml_config

import (
	"fmt"
	"github.com/fsnotify/fsnotify"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"time"
	"yooome/core/container"
	"yooome/global/syserrors"
	"yooome/global/variable"
	"yooome/utils/yml_config/ymlconfig_interface"
)

type ymlConfig struct {
	viper *viper.Viper
}

var lastChangeTime time.Time

func init() {
	lastChangeTime = time.Now()
}
func NewYamlFileFactory(fileName ...string) ymlconfig_interface.YmlConfigInterface {
	yamlConfig := viper.New()
	// 配置文件所在目录
	yamlConfig.AddConfigPath(variable.BasePath + "/config")
	// 需要读取的文件名默认为config
	if len(fileName) == 0 {
		yamlConfig.SetConfigName("config")
	} else {
		yamlConfig.SetConfigName(fileName[0])
	}
	// 设置配置文件类型
	yamlConfig.SetConfigType("yml")
	if err := yamlConfig.ReadInConfig(); err != nil {
		logrus.Error(syserrors.ErrorsConfigInitFail + err.Error())
	}
	config := &ymlConfig{
		yamlConfig,
	}
	return config
}

// 监听文件变化
func (y *ymlConfig) ConfigFileChangeListen() {
	y.viper.OnConfigChange(func(changeEvent fsnotify.Event) {
		if time.Now().Sub(lastChangeTime).Seconds() >= 1 {
			if changeEvent.Op.String() == "WRITE" {
				y.clearCache()
				lastChangeTime = time.Now()
			}
		}
	})
}

func (y *ymlConfig) Clone(fileName string) ymlconfig_interface.YmlConfigInterface {
	// 这里存在一个深拷贝，需要注意，避免拷贝的结构体操作对原始结构体造成影响
	var ymlC = *y
	var ymlConfViper = *(y.viper)
	(&ymlC).viper = &ymlConfViper

	(&ymlC).viper.SetConfigName(fileName)
	if err := (&ymlC).viper.ReadInConfig(); err != nil {
		fmt.Println("clone 失败 ....")
	}
	return &ymlC
}
func (y *ymlConfig) Get(keyName string) interface{} {
	if y.keyIsCache(keyName) {
		return y.getValueFromCache(keyName)
	} else {
		value := y.viper.Get(keyName)
		y.cache(keyName,value)
		return value
	}
}
func (y *ymlConfig) GetString(keyName string) string {
	if y.keyIsCache(keyName) {
		return y.getValueFromCache(keyName).(string)
	}else {
		value := y.viper.GetString(keyName)
		return value
	}
}
func (y *ymlConfig) GetBool(keyName string) bool {
	if  y.keyIsCache(keyName){
		return y.getValueFromCache(keyName).(bool)
	}else {
		value := y.viper.GetBool(keyName)
		return value
	}
}
func (y *ymlConfig) GetInt8(keyName string) int8 {
	if y.keyIsCache(keyName) {
		return y.getValueFromCache(keyName).(int8)
	}else {
		value := y.viper.GetInt(keyName)
		return int8(value)
	}
}
func (y *ymlConfig) GetInt(keyName string) int {
	if y.keyIsCache(keyName) {
		return y.getValueFromCache(keyName).(int)
	}else {
		value := y.viper.GetInt(keyName)
		return value
	}
}
func (y *ymlConfig) GetInt32(keyName string) int32 {
	if y.keyIsCache(keyName) {
		return y.getValueFromCache(keyName).(int32)
	}else {
		value := y.viper.GetInt32(keyName)
		return value
	}
}
func (y *ymlConfig) GetInt64(keyName string) int64 {
	if y.keyIsCache(keyName) {
		return y.getValueFromCache(keyName).(int64)
	}else {
		value := y.viper.GetInt64(keyName)
		return value
	}
}
func (y *ymlConfig) GetFloat64(keyName string) float64 {
	if y.keyIsCache(keyName) {
		return y.getValueFromCache(keyName).(float64)
	}else {
		value := y.viper.GetFloat64(keyName)
		return value
	}
}
func (y *ymlConfig) GetDuration(keyName string) time.Duration {
	if y.keyIsCache(keyName) {
		return y.getValueFromCache(keyName).(time.Duration)
	}else {
		value := y.viper.GetDuration(keyName)
		return value
	}
}
func (y *ymlConfig) GetIntSlice(keyName string) []int  {
	if y.keyIsCache(keyName) {
		return y.getValueFromCache(keyName).([]int)
	}else {
		value := y.viper.GetIntSlice(keyName)
		return value
	}
}
func (y *ymlConfig) GetStringSlice(keyName string) []string {
	if y.keyIsCache(keyName) {
		return y.getValueFromCache(keyName).([]string)
	} else {
		value := y.viper.GetStringSlice(keyName)
		return value
	}
	return nil
}

func (y *ymlConfig) clearCache() {
	fmt.Println("监听文件发生变化.....")
}

func (y *ymlConfig) keyIsCache(name string) bool {
	if _, exists := container.NewContainersFactory().KeyIsExists(name); exists {
		return exists
	} else {
		return exists
	}
}

func (y *ymlConfig) getValueFromCache(name string) interface{} {
	return container.NewContainersFactory().Get(variable.ConfigKeyPrefix + name)
}

func (y *ymlConfig) cache(name string, value interface{}) bool{
	return container.NewContainersFactory().Set(variable.ConfigKeyPrefix+name, value)
}
