package ymlconfig_interface

import "time"

// 由于viper 包本身对于文件的变化事件有一个bug，相关时间会被回调两次
// 常年位彻底解决，相关的 issue  清单 https://github.com/spf13/viper/issues?q=OnConfigChange
// 设置一个内部全局变量，记录文件文件变化时间点，如果两次回调事件时间差 小于1秒，我们认为是第二次回调事件，而不是人工修改配置文件
// 这样就避免了 viper 包的这个bug
type YmlConfigInterface interface {
	ConfigFileChangeListen()
	Clone(fileName string) YmlConfigInterface
	Get(keyName string) interface{}
	GetString(keyName string) string
	GetBool(keyName string) bool
	GetInt8(keyName string) int8
	GetInt(keyName string) int
	GetInt32(keyName string) int32
	GetInt64(keyName string) int64
	GetFloat64(keyName string) float64
	GetDuration(keyName string) time.Duration
	GetStringSlice(keyName string) []string
	GetIntSlice(keyName string) []int
}
