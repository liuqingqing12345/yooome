package yset

import "yooome/utils/rwmutex"

type Set struct {
	mu   rwmutex.RWMutex
	data map[interface{}]struct{}
}

func NewSet(safe ...bool) *Set {
	return &Set{
		data: make(map[interface{}]struct{}),
		mu: rwmutex.Create(safe...),
	}
}
