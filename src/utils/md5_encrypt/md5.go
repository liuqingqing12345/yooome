package md5_encrypt

import (
	"crypto/md5"
	"fmt"
	"yooome/utils/convert"
)

func Encrypt(data interface{}) (encrypt string, err error) {
	return EncryptBytes(convert.Bytes(data))
}
func MustEncrypt(data interface{}) string {
	result,err := Encrypt(data)
	if err != nil {
		panic(err)
	}
	return result
}
func EncryptBytes(data []byte) (string, error) {
	h := md5.New()
	if _, err := h.Write(data); err != nil {
		return "", err
	}
	return fmt.Sprintf("%x", h.Sum(nil)), nil
}
func MustEncryptBytes(data []byte) string {
	result,err := EncryptBytes(data)
	if err != nil {
		panic(err)
	}
	return result
}
func MustEncryptString(data string) string  {
	result,err := EncryptString(data)
	if err != nil {
		panic(err)
	}
	return result
}
func EncryptString(data string) (encrypt string,err error) {
	return EncryptBytes([]byte(data))
}
