package cache

import (
	"context"
	"fmt"
	"yooome/utils/convert"
)

type Cache struct {
	adapter Adapter         // Adapter for cache feature
	context context.Context // Context for options
}

func NewCache(lruCap ...int) *Cache {
	memoryAdapter := newAdapterMemory(lruCap...)
	c := &Cache{
		adapter: memoryAdapter,
	}
	fmt.Println(c)
	return c
}

// clone return a shallow copy of current object
func (c *Cache) Clone() *Cache {
	return &Cache{
		adapter: c.adapter,
		context: c.context,
	}
}

//
func (c *Cache) Ctx(ctx context.Context) *Cache {
	newCache := c.Clone()
	newCache.context = ctx
	return newCache
}

// SetAdapter change the adapter for this Cache
func (c *Cache) SetAdapter(adapter Adapter) {
	c.adapter = adapter
}

func (c *Cache) Removes(keys []interface{}) error {
	_, err := c.Remove(keys...)
	return err
}
func (c *Cache) KeyStrings() ([]string, error) {
	keys, err := c.Keys()
	if err != nil {
		return nil, err
	}
	return convert.StrToSliceStr(keys), nil
}
func (c *Cache) getCtx() context.Context {
	if c.context == nil {
		return context.Background()
	}
	return c.context
}


