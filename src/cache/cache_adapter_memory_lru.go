package cache

import (
	"yooome/utils/safelist"
	"yooome/utils/utilsbool"
	"yooome/utils/utilsmap"
	"yooome/utils/ytime"
)

type adapterMemoryLru struct {
	cache   *adapterMemory
	data    *utilsmap.AnyAnyMap
	list    *safelist.SafeList
	rawList *safelist.SafeList
	closed  *utilsbool.Bool
}

func NewAdapterMemoryLru(c *adapterMemory) *adapterMemoryLru {
	lru := &adapterMemoryLru{
		cache:  c,
		data:   utilsmap.NewAnyAnyMap(true),
		list:   safelist.New(true),
		closed: utilsbool.NewBool(),
	}
	return lru
}

// Close closes the LRU object.
func (lru *adapterMemoryLru) Close() {
	lru.closed.Set(true)
}
func (lru *adapterMemoryLru) Remove(key interface{}) {
	if v := lru.data.Get(key); v != nil {
		lru.data.Remove(key)
		lru.list.Remove(v.(*safelist.Element))
	}
}
func (lru *adapterMemoryLru) Size() int {
	return lru.data.Size()
}
func (lru *adapterMemoryLru) Push(key interface{}) {
	lru.rawList.PushBack(key)
}
func (lru *adapterMemoryLru) Pop() interface{} {
	if v := lru.list.PopBack(); v != nil {
		lru.data.Remove(v)
		return v
	}
	return nil
}
func (lru *adapterMemoryLru) SyncAndClear()  {
	if lru.closed.Val() {
		ytime.Exit()
		return
	}
	for {
		if v := lru.rawList.PopFront(); v != nil {
			// Deleting the key from list.
			if v := lru.data.Get(v); v != nil {
				lru.list.Remove(v.(*safelist.Element))
			}
			// Pushing key to the head of the list
			// and setting its list item to hash table for quick indexing.
			lru.data.Set(v, lru.list.PushFront(v))
		} else {
			break
		}
	}
	// Data cleaning up.
	for i := lru.Size() - lru.cache.cap; i > 0; i-- {
		if s := lru.Pop(); s != nil {
			lru.cache.clearByKey(s, true)
		}
	}
}
