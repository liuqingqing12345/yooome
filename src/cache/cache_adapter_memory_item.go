package cache

import "time"

func (item *adapterMemoryItem) IsExpired() bool {
	if item.e >= time.Now().UnixNano()/1e6 {
		return false
	}
	return true
}
