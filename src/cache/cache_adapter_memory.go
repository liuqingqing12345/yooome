package cache

import (
	"context"
	"time"
	"yooome/utils/safelist"
	"yooome/utils/ymtype"
	"yooome/utils/ytime"
)

type adapterMemory struct {
	cap         int
	data        *adapterMemoryData
	expireTimes *adapterMemoryExpireTime
	expireSets  *adapterMemoryExpireSet
	lru         *adapterMemoryLru
	lruGetList  *safelist.SafeList
	eventList   *safelist.SafeList
	closed      *ymtype.Bool
}
type adapterMemoryItem struct {
	v interface{}
	e int64
}
type adapterMemoryEvent struct {
	k interface{} // key
	e int64       //Expire time in milliseconds.
}

const (
	// defaultMaxExpire is the default expire time for no expiring items.
	// It equals to math.MaxInt64/1000000.
	defaultMaxExpire = 9223372036854
)

func newAdapterMemory(lruCap ...int) *adapterMemory {
	c := &adapterMemory{
		data:        newAdapterMemoryData(),
		lruGetList:  safelist.New(true),
		expireTimes: newAdapterMemoryExpireTimes(),
		expireSets:  newAdapterMemoryExpireSets(),
		eventList:   safelist.New(true),
		closed:      ymtype.NewBool(),
	}
	if len(lruCap) > 0 {
		c.cap = lruCap[0]
		c.lru = NewAdapterMemoryLru(c)
	}
	return c
}

func (d *adapterMemory) Set(ctx context.Context, key interface{}, val interface{}, duration time.Duration) error {
	expireTime := d.getInternalExpire(duration)
	d.data.Set(key, adapterMemoryItem{
		v: val,
		e: expireTime,
	})
	d.eventList.PushBack(&adapterMemoryEvent{
		k: key,
		e: expireTime,
	})
	return nil
}
func (d *adapterMemory) Sets(ctx context.Context, data map[interface{}]interface{}, duration time.Duration) error {
	expireTime := d.getInternalExpire(duration)
	err := d.data.Sets(data, expireTime)
	if err != nil {
		return err
	}
	for k, _ := range data {
		d.eventList.PushBack(&adapterMemoryEvent{
			k: k,
			e: expireTime,
		})
	}
	return nil
}
func (d *adapterMemory) SetIfNotExist(ctx context.Context, key interface{}, value interface{}, duration time.Duration) (bool, error) {
	isContained, err := d.Contains(ctx, key)
	if err != nil {
		return false, err
	}
	if !isContained {
		_, err := d.doSetWithLockCheck(key, value, duration)
		if err != nil {
			return false, err
		}
		return true, nil
	}
	return false, nil
}
func (d *adapterMemory) Get(context context.Context, key interface{}) (interface{}, error) {
	item, ok := d.data.Get(key)
	if ok && !item.IsExpired() {
		if d.cap > 0 {
			d.lruGetList.PushBack(key)
		}
		return item.v, nil
	}
	return nil, nil
}
func (d *adapterMemory) GetOrSet(ctx context.Context, key interface{}, value interface{}, duration time.Duration) (interface{}, error) {
	v, err := d.Get(ctx, key)
	if err != nil {
		return nil, err
	}
	if v == nil {
		return d.doSetWithLockCheck(key, value, duration)
	} else {
		return v, nil
	}
}
func (d *adapterMemory) GetOrSetFun(ctx context.Context, key interface{}, f func() (interface{}, error), duration time.Duration) (interface{}, error) {
	v, err := d.Get(ctx, key)
	if err != nil {
		return nil, err
	}
	if v == nil {
		value, err := f()
		if err != nil {
			return nil, err
		}
		if value == nil {
			return nil, nil
		}
		return d.doSetWithLockCheck(key, value, duration)
	} else {
		return v, nil
	}
}
func (d *adapterMemory) GetOrSetFuncLock(ctx context.Context, key interface{}, f func() (interface{}, error), duration time.Duration) (interface{}, error) {
	v, err := d.Get(ctx, key)
	if err != nil {
		return nil, err
	}
	if v == nil {
		return d.doSetWithLockCheck(key, f, duration)
	} else {
		return v, nil
	}
}

// Contains returns true if <key> exists in the cache, or else returns false.
func (d *adapterMemory) Contains(ctx context.Context, key interface{}) (bool, error) {
	v, err := d.Get(ctx, key)
	if err != nil {
		return false, err
	}
	return v != nil, nil
}
func (d *adapterMemory) GetExpire(ctx context.Context, key interface{}) (time.Duration, error) {
	if item, ok := d.data.Get(key); ok {
		return time.Duration(item.e-ytime.TimestampMilli()) * time.Millisecond, nil
	}
	return -1, nil
}
func (d *adapterMemory) Remove(ctx context.Context, keys ...interface{}) (value interface{}, err error) {
	var removeKeys []interface{}
	removeKeys, value, err = d.data.Remove(keys...)
	if err != nil {
		return
	}
	for _, key := range removeKeys {
		d.eventList.PushBack(&adapterMemoryEvent{
			k: key,
			e: ytime.TimestampMilli() - 1000000,
		})
	}
	return
}
func (d *adapterMemory) Update(ctx context.Context, key interface{}, value interface{}) (oldValue interface{}, exist bool, err error) {
	return d.data.Update(key, value)
}
func (d *adapterMemory) UpdateExpire(ctx context.Context, key interface{}, duration time.Duration) (oldDuration time.Duration, err error) {
	newExpireTime := d.getInternalExpire(duration)
	oldDuration, err = d.data.UpdateExpire(key, newExpireTime)
	if err != nil {
		return
	}
	if oldDuration != -1 {
		d.eventList.PushBack(&adapterMemoryEvent{
			key,
			newExpireTime,
		})
	}
	return
}
func (d *adapterMemory) Data(ctx context.Context) (map[interface{}]interface{}, error) {
	return d.data.Data()
}
func (d *adapterMemory) Keys(ctx context.Context) ([]interface{}, error) {
	return d.data.Keys()
}
func (d *adapterMemory) Values(ctx context.Context) ([]interface{}, error) {
	return d.data.Values()
}
func (d *adapterMemory) Size(ctx context.Context) (size int, err error) {
	return d.data.Size()
}
func (d *adapterMemory) Clear(ctx context.Context) error {
	return d.data.Clear()
}
func (d *adapterMemory) Close(ctx context.Context) error {
	if d.cap > 0 {
		d.lru.Close()
	}
	d.closed.Set(true)
	return nil
}
func (d *adapterMemory) getInternalExpire(duration time.Duration) int64 {
	if duration == 0 {
		return defaultMaxExpire
	} else {
		return ytime.TimestampMilli() + duration.Nanoseconds()/1e6
	}
}
func (d *adapterMemory) doSetWithLockCheck(key interface{}, value interface{}, duration time.Duration) (result interface{}, err error) {
	expireTimestamp := d.getInternalExpire(duration)
	result, err = d.data.SetWithLock(key, value, expireTimestamp)
	d.eventList.PushBack(&adapterMemoryEvent{k: key, e: expireTimestamp})
	return
}

func (d *adapterMemory) clearByKey(s interface{}, b bool) {
	return
}
