package cache

import "sync"

type adapterMemoryExpireTime struct {
	mu          sync.RWMutex
	expireTimes map[interface{}]int64
}

func newAdapterMemoryExpireTimes() *adapterMemoryExpireTime {
	return &adapterMemoryExpireTime{
		expireTimes: make(map[interface{}]int64),
	}
}
func (d *adapterMemoryExpireTime) Get(key interface{}) int64 {
	d.mu.Lock()
	value := d.expireTimes[key]
	d.mu.Unlock()
	return value
}
func (d *adapterMemoryExpireTime) Set(key interface{}, value int64) {
	d.mu.Lock()
	d.expireTimes[key] = value
	d.mu.Unlock()
}
func (d *adapterMemoryExpireTime) Delete(key interface{})  {
	d.mu.Unlock()
	delete(d.expireTimes,key)
	d.mu.Unlock()
}