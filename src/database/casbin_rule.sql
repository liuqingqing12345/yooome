-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: gfast
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `casbin_rule`
--

DROP TABLE IF EXISTS `casbin_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `casbin_rule` (
  `ptype` varchar(10) DEFAULT NULL,
  `v0` varchar(256) DEFAULT NULL,
  `v1` varchar(256) DEFAULT NULL,
  `v2` varchar(256) DEFAULT NULL,
  `v3` varchar(256) DEFAULT NULL,
  `v4` varchar(256) DEFAULT NULL,
  `v5` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `casbin_rule`
--

LOCK TABLES `casbin_rule` WRITE;
/*!40000 ALTER TABLE `casbin_rule` DISABLE KEYS */;
INSERT INTO `casbin_rule` VALUES ('p','g_8','r_1','All','','',''),('p','g_8','r_5','All','','',''),('p','g_8','r_19','All','','',''),('p','g_8','r_20','All','','',''),('p','g_8','r_21','All','','',''),('p','g_8','r_22','All','','',''),('p','g_8','r_23','All','','',''),('p','g_8','r_24','All','','',''),('p','g_8','r_25','All','','',''),('p','g_8','r_6','All','','',''),('p','g_1','r_1','All','','',''),('p','g_1','r_5','All','','',''),('p','g_1','r_19','All','','',''),('p','g_1','r_20','All','','',''),('p','g_1','r_21','All','','',''),('p','g_1','r_22','All','','',''),('p','g_1','r_23','All','','',''),('p','g_1','r_24','All','','',''),('p','g_1','r_25','All','','',''),('p','g_1','r_6','All','','',''),('p','g_1','r_2','All','','',''),('p','g_1','r_8','All','','',''),('p','g_1','r_16','All','','',''),('p','g_1','r_17','All','','',''),('p','g_1','r_18','All','','',''),('p','g_1','r_26','All','','',''),('p','g_1','r_27','All','','',''),('p','g_1','r_29','All','','',''),('p','g_1','r_30','All','','',''),('p','g_1','r_31','All','','',''),('p','g_1','r_28','All','','',''),('p','g_1','r_33','All','','',''),('p','g_1','r_40','All','','',''),('p','g_1','r_3','All','','',''),('p','g_1','r_34','All','','',''),('p','g_1','r_36','All','','',''),('p','g_1','r_37','All','','',''),('p','g_1','r_38','All','','',''),('p','g_1','r_39','All','','',''),('p','g_1','r_4','All','','',''),('p','g_1','r_32','All','','',''),('p','g_1','r_35','All','','',''),('g','u_1','g_1','','','',''),('g','u_1','g_3','','','',''),('g','u_22','g_2','','','',''),('g','u_22','g_1','','','',''),('g','u_31','g_1','','','',''),('g','u_31','g_2','','','',''),('g','u_15','g_2','','','',''),('g','u_16','g_3','','','',''),('g','u_3','g_2','','','',''),('p','g_2','r_1','All','','',''),('p','g_2','r_5','All','','',''),('p','g_2','r_19','All','','',''),('p','g_2','r_20','All','','',''),('p','g_2','r_21','All','','',''),('p','g_2','r_22','All','','',''),('p','g_2','r_23','All','','',''),('p','g_2','r_24','All','','',''),('p','g_2','r_25','All','','',''),('p','g_2','r_6','All','','',''),('p','g_2','r_41','All','','',''),('p','g_2','r_42','All','','',''),('p','g_2','r_43','All','','',''),('p','g_2','r_2','All','','',''),('p','g_2','r_8','All','','',''),('p','g_2','r_16','All','','',''),('p','g_2','r_17','All','','',''),('p','g_2','r_18','All','','',''),('p','g_2','r_26','All','','',''),('p','g_2','r_27','All','','',''),('p','g_2','r_29','All','','',''),('p','g_2','r_30','All','','',''),('p','g_2','r_31','All','','',''),('p','g_2','r_28','All','','',''),('p','g_2','r_33','All','','',''),('p','g_2','r_40','All','','',''),('p','g_2','r_3','All','','',''),('p','g_2','r_34','All','','',''),('p','g_2','r_36','All','','',''),('p','g_2','r_37','All','','',''),('p','g_2','r_38','All','','',''),('p','g_2','r_39','All','','',''),('p','g_2','r_4','All','','',''),('p','g_2','r_32','All','','',''),('p','g_2','r_35','All','','',''),('g','u_2','g_1','','','','');
/*!40000 ALTER TABLE `casbin_rule` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-26 20:22:06
