-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: gfast
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sys_dict_data`
--

DROP TABLE IF EXISTS `sys_dict_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sys_dict_data` (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) DEFAULT '0' COMMENT '字典排序',
  `dict_label` varchar(100) DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) DEFAULT NULL COMMENT '表格回显样式',
  `is_default` tinyint(1) DEFAULT '0' COMMENT '是否默认（1是 0否）',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` bigint(64) unsigned DEFAULT '0' COMMENT '创建者',
  `create_time` bigint(20) unsigned DEFAULT '0' COMMENT '创建时间',
  `update_by` bigint(64) unsigned DEFAULT '0' COMMENT '更新者',
  `update_time` bigint(20) unsigned DEFAULT '0' COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=COMPACT COMMENT='字典数据表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict_data`
--

LOCK TABLES `sys_dict_data` WRITE;
/*!40000 ALTER TABLE `sys_dict_data` DISABLE KEYS */;
INSERT INTO `sys_dict_data` VALUES (1,0,'男','1','sys_user_sex','','',0,1,31,1582884113,31,1593656868,'备注信息'),(2,0,'女','2','sys_user_sex','','',0,1,31,1582884125,31,1593656871,'备注信息'),(3,0,'保密','0','sys_user_sex','','',1,1,31,1582884871,31,1593656875,'备注信息'),(24,0,'频道页','1','cms_category_type','','',0,1,31,1583131942,31,1592532634,'作为频道页，不可作为栏目发布文章，可添加下级分类'),(25,0,'发布栏目','2','cms_category_type','','',0,1,31,1583132032,31,1592532724,'作为发布栏目，可添加文章'),(26,0,'跳转栏目','3','cms_category_type','','',0,1,31,1583132125,31,1592532737,'不直接发布内容，用于跳转页面'),(27,0,'单页栏目','4','cms_category_type','','',0,1,31,1583132145,31,1592532755,'单页面模式，分类直接显示为文章'),(28,0,'正常','0','sys_job_status','','default',1,1,31,1583762727,0,0,''),(29,0,'暂停','1','sys_job_status','','default',0,1,31,1583762751,31,1583763095,''),(32,0,'成功','1','admin_login_status','','default',0,1,31,1583891238,31,1583891244,''),(33,0,'失败','0','admin_login_status','','default',0,1,31,1583891262,0,0,''),(34,0,'成功','1','sys_oper_log_status','','default',0,1,31,1583917929,0,0,''),(35,0,'失败','0','sys_oper_log_status','','default',0,1,31,1583917942,0,0,''),(36,0,'重复执行','1','sys_job_policy','','default',1,1,31,1584687209,0,0,''),(37,0,'执行一次','2','sys_job_policy','','default',1,1,31,1584687226,0,0,''),(38,0,'显示','1','sys_show_hide',NULL,'default',1,1,31,1584687226,0,0,NULL),(39,0,'隐藏','0','sys_show_hide',NULL,'default',0,1,31,1584687226,0,0,NULL),(40,0,'正常','1','sys_normal_disable','','default',1,1,31,1592214217,0,0,''),(41,0,'停用','0','sys_normal_disable','','default',0,1,31,1592214239,0,0,''),(49,0,'是','1','sys_yes_no','','',1,1,31,1592381742,0,1592381742,''),(50,0,'否','0','sys_yes_no','','',0,1,31,1592381753,0,1592381753,''),(51,0,'已发布','1','cms_news_pub_type','','',1,1,31,1593336428,31,1593393871,''),(54,0,'未发布','0','cms_news_pub_type','','',0,1,31,1593393852,0,1593393852,''),(55,0,'置顶','1','cms_news_attr','','',0,1,31,1593394753,0,1593394753,''),(56,0,'推荐','2','cms_news_attr','','',0,1,31,1593394762,0,1593394762,''),(57,0,'普通文章','0','cms_news_type','','',0,1,31,1593397458,31,1593399098,''),(58,0,'跳转链接','1','cms_news_type','','',0,1,31,1593397468,31,1593399105,''),(59,0,'cms模型','6','cms_cate_models','','',0,1,1,1595495461,1,1595580310,''),(64,0,'幻灯','3','cms_news_attr','','',0,1,31,1597648857,0,1597648857,''),(65,0,'[work]测试业务表','wf_news','flow_type','','',0,1,31,1606966815,0,1606966815,''),(66,0,'回退修改','-1','flow_status','','',0,1,31,1606966849,0,1606966849,''),(67,0,'保存中','0','flow_status','','',0,1,31,1606966868,0,1606966868,''),(68,0,'流程中','1','flow_status','','',0,1,31,1606966885,0,1606966885,''),(69,0,'审批通过','2','flow_status','','',0,1,31,1606966898,0,1606966898,''),(70,0,'普通','0','flow_level','','',0,1,31,1606966947,0,1606966947,''),(71,0,'加急','1','flow_level','','',0,1,31,1606967141,0,1606967141,''),(72,0,'紧急','2','flow_level','','',0,1,31,1606967151,0,1606967151,''),(73,0,'特急','3','flow_level','','',0,1,31,1606967161,0,1606967161,'');
/*!40000 ALTER TABLE `sys_dict_data` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-04 13:29:00
