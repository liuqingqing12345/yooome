package token

import (
	"encoding/base64"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"strings"
	"time"
	"yooome/utils/convert"
	"yooome/utils/crypto/aes"
	"yooome/utils/md5_encrypt"
	"yooome/utils/random"
	"yooome/utils/yml_config"
	"yooome/utils/yml_config/ymlconfig_interface"
	"yooome/utils/ytime"
)

const (
	CacheModelCache = 1
	CacheModelRedis = 2
)

var configYml ymlconfig_interface.YmlConfigInterface

func init() {
	configYml = yml_config.NewYamlFileFactory()
}

// mToken 结构体
type MToken struct {
	// Server Name
	ServerName string
	// 缓存模式
	CacheMode int8
	// 缓存key
	CacheKey string
	// 超时时间 默认是十天
	TimeOut int
	// 缓存刷新时间 默认为超时时间的一半
	MaxRefresh int
	// Token 分隔符
	TokenDelimiter string
	// Token加密
	EncryptKey []byte
	// 认证失败中文提示
	AuthFailMsg string
	// 多端登录
	MultiLogin bool
	// 中间件类型 1 GroupMiddleware 2 BindMiddleware
	MiddleWareType uint
	// 登陆路径
	LoginPath string
	// 退出路径
	LogoutPath string
}

func NewMToken() *MToken {
	mToken := &MToken{
		ServerName:     "佑米科技",
		CacheMode:      configYml.GetInt8("MToken.CacheMode"),
		CacheKey:       configYml.GetString("MToken.CacheKey"),
		TimeOut:        10 * 24 * 60 * 60 * 1000,
		MaxRefresh:     configYml.GetInt("MToken.MaxRefresh"),
		TokenDelimiter: configYml.GetString("MToken.TokenDelimiter"),
		EncryptKey:     []byte(configYml.GetString("MToken.EncryptKey")),
		AuthFailMsg:    configYml.GetString("MToken.AuthFailMsg"),
		MultiLogin:     configYml.GetBool("MToken.MultiLogin"),
		MiddleWareType: 2,
		LoginPath:      "/sysLogin/login",
		LogoutPath:     "/sysLogin/logout",
	}
	return mToken
}
func (m *MToken) InitConfig() bool {
	if m.CacheMode == 0 {
		m.CacheMode = CacheModelCache
	}
	if m.CacheKey == "" {
		m.CacheKey = "GToken:"
	}
	if m.TimeOut == 0 {
		m.TimeOut = 10 * 24 * 60 * 60 * 1000
	}
	if m.MaxRefresh == 0 {
		m.MaxRefresh = m.TimeOut / 2
	}
	if m.TokenDelimiter == "" {
		m.TokenDelimiter = "_"
	}
	if len(m.EncryptKey) == 0 {
		m.EncryptKey = []byte("koi29a83idakguqjq29asd9asd8a7jhq")
	}
	if m.AuthFailMsg == "" {
		m.AuthFailMsg = "请求或者登陆超时"
	}
	// 设置中间件模式，未设置说明历史版本，通过GlobalMiddleware兼容
	//if m.MiddlewareType == 0 {
	//	if m.GlobalMiddleware {
	//		m.MiddlewareType = MiddlewareTypeGlobal
	//	} else {
	//		m.MiddlewareType = MiddlewareTypeBind
	//	}
	//}
	return true
}

// 生成 token
func (m *MToken) GenToken(userKey string, data interface{}) Resp {
	token := m.EncryptToken(userKey, "")
	if !token.Success() {
		return token
	}
	cacheKey := m.CacheKey + userKey
	userCache := map[string]interface{}{
		"userKey":     userKey,
		"uuid":        token.GetString("uuid"),
		"data":        data,
		"createTime":  time.Now().UnixNano() / 1e6,
		"refreshTime": time.Now().UnixNano() + convert.Int64(m.MaxRefresh),
	}
	// MToken 中设置
	cacheResp := m.setCache(cacheKey, userCache)
	if !cacheResp.Success() {
		return cacheResp
	}
	return token
}

// GetToken 获取token
func (m *MToken) GetToken(userKey string) Resp {
	cacheKey := m.CacheKey + userKey
	userCacheResp := m.getCache(cacheKey)
	if !userCacheResp.Success() {
		return userCacheResp
	}
	userCache := convert.Map(userCacheResp.Data)
	nowTime := ytime.Now().TimestampMilli()
	refreshTime := userCache["refreshTime"]

	// 需要进行缓存超时时间刷新
	if convert.Int64(refreshTime) == 0 || nowTime > convert.Int64(refreshTime) {
		userCache["createTime"] = ytime.Now().TimestampMilli()
		userCache["refreshTime"] = ytime.Now().TimestampMilli() + convert.Int64(m.MaxRefresh)
		logrus.Info("[GToken]refreshToken:" + convert.String(userCache))
		return m.setCache(cacheKey, userCache)
	}
	return Succ(userCache)
}

func (m *MToken) EncryptToken(userKey string, uuid string) Resp {
	if userKey == "" {
		return Fail("encrypt userKey empty")
	}
	if uuid == "" {
		// 重新生成uuid
		newUuid, err := md5_encrypt.Encrypt(random.Letters(10))
		if err != nil {
			logrus.Error("[MToken]uuid error", err)
			return Error("uuid error")
		}
		uuid = newUuid
	}
	tokenStr := userKey + m.TokenDelimiter + uuid
	token, err := aes.Encrypt([]byte(tokenStr), m.EncryptKey)
	if err != nil {
		logrus.Error("aes encrypt error: ", err)
		return Error("aes encrypt error")
	}
	tokenMap := map[string]interface{}{
		"userKey": userKey,
		"uuid":    uuid,
		"token":   base64.StdEncoding.EncodeToString([]byte(token)),
	}
	return Succ(tokenMap)
}

// removeToken 删除token
func (m *MToken) RemoveToken(token string) Resp {
	decryptToken := m.DecryptToken(token)
	if !decryptToken.Success() {
		return decryptToken
	}
	cacheKey := m.CacheKey + decryptToken.GetString("userKey")
	return m.removeCache(cacheKey)
}

//DecryptToken token 解密方法
func (m *MToken) DecryptToken(token string) Resp {
	if token == "" {
		return Fail("decrypt token empty")
	}
	result, err := base64.StdEncoding.DecodeString(token)
	if err != nil {
		logrus.Error("[MToken] Decode error ", err)
		return Fail("Decode Token error ")
	}
	decryptToken, err2 := aes.Decrypt(result, m.EncryptKey)
	fmt.Println("decryptToken=", string(decryptToken))
	if err2 != nil {
		logrus.Error("aes decrypt error", err2)
		return Error("[aes] decrypt error")
	}
	mTokenArray := strings.Split(string(decryptToken), m.TokenDelimiter)
	if len(mTokenArray) < 2 {
		logrus.Error("token len error")
		return Error("token len error")
	}
	return Succ(map[string]interface{}{
		"userKey": mTokenArray[0],
		"uuid":    mTokenArray[1],
	})
}
func (m *MToken) Start() bool {
	if !m.InitConfig() {
		return false
	}
	logrus.Info("[MToken][params:" + m.String() + "]start...")

	return true
}

func (m *MToken) String() string {
	return ""
}
func (m *MToken) GetTokenData(context *gin.Context) Resp {
	// 获取 url 传递过来的
	result := m.getRequestToken(context)
	if result.Success() {
		// 验证 token 是否正确
		result = m.validToken(result.Data)
		fmt.Println(result)
	}
	// 返回 token 结果
	return result
}
func (m *MToken) getRequestToken(context *gin.Context) Resp {
	authorization := context.Request.Header.Get("Authorization")
	if authorization != "" {
		parts := strings.SplitN(authorization, " ", 2)
		if !(len(parts) == 2 && parts[0] == "Bearer") {
			logrus.Error("authHeader error ", authorization)
			return Unauthorized("get url token key fail", "")
		} else if parts[1] == "" {
			logrus.Error("authHeader Token error", parts[1])
			return Unauthorized("get auth herder token error ", parts[1])
		}
		return Succ(parts[1])
	}
	return Error("get request token error")
}

func (m *MToken) validToken(data interface{}) Resp {
	// 转化为string类型
	isToken := convert.String(data)
	if isToken == "" {
		return Unauthorized("valid token empty", "")
	}
	// token进行解密
	decryptToken := m.DecryptToken(isToken)
	if !decryptToken.Success() {
		return decryptToken
	}
	// 将 decryptToken interface 类型转化为map类型
	covertToken := convert.Map(decryptToken.Data)

	userKey := covertToken["userKey"]
	uuid := covertToken["uuid"]
	// 根据userKey 获取缓存的 token
	userCacheToken := m.GetToken(convert.String(userKey))
	if !userCacheToken.Success() {
		logrus.Error("user cache token error ,please check cache token!!!")
		return Unauthorized("user cache token error", "")
	}
	cacheToken := convert.Map(userCacheToken.Data)
	if uuid != cacheToken["uuid"] {
		return Error("cache token uuid error")
	}
	// result := m.GetToken(convert.String(userKey))
	return userCacheToken
}
