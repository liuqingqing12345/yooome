package token

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"time"
	"yooome/cache"
	"yooome/utils/connect"
	"yooome/utils/convert"
)
// setCache 设置缓存
func (m *MToken) setCache(cacheKey string, userCache map[string]interface{}) Resp {
	switch m.CacheMode {
	case CacheModelCache:
		_ = cache.Set(cacheKey, userCache, convert.Duration(m.TimeOut)*time.Millisecond)
	case CacheModelRedis:
		cacheKeyJson, err := json.Marshal(userCache)
		if err != nil {
			logrus.Info("[GToken]cache json encode error ", cacheKeyJson)
			return Error("cache json encode error")
		}
		// 使用redis进行缓存
		redis := connect.InitRedis()
		_, err = redis.Do("SETEX", cacheKey, m.TimeOut/1000, cacheKeyJson)
		if err != nil {
			logrus.Error("MToken cache set error ", err)
			return Error("cache set error")
		}
	default:
		Error(" cache set error ")
	}
	return Succ(userCache)
}

// getCache 获取缓存
func (m *MToken) getCache(cacheKey string) Resp {
	var userCache map[string]interface{}
	switch m.CacheMode {
	case CacheModelCache:
		userCacheValue, err := cache.Get(cacheKey)
		if err != nil {
			logrus.Error("[MToken] cache err = ", err)
			return Error("cache ge err = ")
		}
		if userCacheValue == nil {
			return Unauthorized("login timeout or not login", "")
		}
		userCache = convert.Map(userCacheValue)
	case CacheModelRedis:
		userCacheJson, err := connect.InitRedis().Do("get", cacheKey)
		if err != nil {
			logrus.Error("cache get error", err)
			return Error("cache get error")
		}
		if userCacheJson == nil {
			return Unauthorized("login time out or not login ", "")
		}
		decoder := json.NewDecoder(bytes.NewReader(convert.Bytes(userCacheJson)))
		err = decoder.Decode(&userCache)
		if err != nil {
			logrus.Error("cache get json error ", err)
			Error("cache get json error")
		}
	default:
		return Error("cache model error")
	}
	fmt.Println("user cacheKey = ", userCache)
	return Succ(userCache)
}
func (m *MToken) removeCache(key string) Resp {
	switch m.CacheMode {
	case CacheModelCache:
		_, _ = cache.Remove(key)
	case CacheModelRedis:
		var err error
		_, err = connect.InitRedis().Do("DEL", key)
		if err != nil {
			logrus.Error("cache remove error", err)
			return Error("cache remove error")
		}
	default:
		return Error("cache model error")
	}
	return Succ("")
}