package token

import "yooome/utils/convert"

const (
	SUCCESS = 1
	FAIL    = -1
	ERROR   = -99
)

type Resp struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

func (resp *Resp) Success() bool {
	return resp.Code == SUCCESS
}

// GetString 获取 string 类型
func (resp *Resp) GetString(key string) string {
	//data := convert.Map(resp.Data)
	//if data == nil {
	//	return ""
	//}
	//convert.String()
	// return data[key]
	return convert.String(resp.Get(key))
}

func (resp *Resp) Get(key string) interface{} {
	res := convert.Map(resp.Data)
	if res == nil {
		return ""
	}
	return res[key]
}

// 认证失败
func Fail(msg string) Resp {
	return Resp{FAIL, msg, ""}
}

// 认证错误
func Error(msg string) Resp {
	return Resp{ERROR, msg, ""}
}

// 认证成功
func Succ(data interface{}) Resp {
	return Resp{SUCCESS, "success", data}
}

// 认证失败
func Unauthorized(msg string, data interface{}) Resp {
	return Resp{ERROR, msg, data}
}
