package type_test

import (
	"fmt"
	"testing"
)

func TestTypeDemo(T *testing.T) {
	str := "zhangsan"
	sliceStr := strTest(str)
	fmt.Println(sliceStr)
}

func strTest(str interface{}) []string {
	var array []string
	switch value := str.(type) {
	case string:
		array = make([]string, len(value))
		for k, v := range value {
			array[k] = string(v)
		}

	}
	return array
}
