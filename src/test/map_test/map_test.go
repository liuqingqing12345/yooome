package map_test

import (
	"fmt"
	"testing"
)

func TestMapDemo(T *testing.T) {
	data := make(map[string]interface{})
	data["k1"] = nil
	data["k2"] = nil
	data["k3"] = ""
	data["k4"] = "k4"
	data["k5"] = "k5"
	for k, val := range data {
		fmt.Println("data["+k+"]", val)
	}
}
