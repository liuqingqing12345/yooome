package test

import (
	"fmt"
	"testing"
	"yooome/token"
)

func TestNewMToken(T *testing.T) {
	tokenCache := token.NewMToken()
	resp := tokenCache.GenToken("newMToken", "tokenCache")
	fmt.Println("resp = ", resp)
}
func TestGetMToken(T *testing.T) {
	tokenCache := token.NewMToken()
	result := tokenCache.GetToken("newMToken")
	fmt.Println("result = ", result)
}
func TestMTokenRemove(T *testing.T) {
	tokenCache := token.NewMToken()
	t := "2mdEZrysnqAfvF1JIftszN3oZJRZqPXib3BNPQ9ZWXCGXL5ZEkcG6PQRbTb3Mpnk"
	result := tokenCache.RemoveToken(t)
	fmt.Println("result = ", result)

}
