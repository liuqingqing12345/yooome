package test

import (
	"fmt"
	"github.com/casbin/casbin/v2"
	adapter "github.com/casbin/gorm-adapter/v3"
	_ "github.com/go-sql-driver/mysql"
	"os"
	"strings"
	"testing"
	"yooome/global/variable"
	initcasbin "yooome/utils/casbin"
	"yooome/utils/convert"
	"yooome/utils/gorminit"
	"yooome/utils/yml_config"
)

func TestCasBinDemo(T *testing.T) {
	a, _ := adapter.NewAdapter("mysql", "root:yooome123@tcp(127.0.0.1:3306)/casbin", true)
	e, _ := casbin.NewEnforcer("../config/rbac_model.conf", a)
	sub := "alice"
	obj := "data1"
	act := "read"
	added, err := e.AddPolicy("alice", "data1", "read")
	_, _ = e.AddGroupingPolicy("bob", "data1", "write")
	result := e.GetFilteredGroupingPolicy(0, "bob")
	fmt.Println("result = ", result)
	if err != nil {
		fmt.Printf("%s", err)
	}
	ok, err := e.Enforce(sub, obj, act)
	fmt.Println(added)
	fmt.Print(err)
	if ok == true {
		fmt.Printf("通过...")
	} else {
		fmt.Println("未通过....")
	}
}
func TestCasBinDemo2(T *testing.T) {
	// 1.检查配置文件是否存在
	if _, err := os.Stat(variable.BasePath + "/config/gorm.yml"); err != nil {
		fmt.Println("配置文件gorm.yml不存在....")
	}
	variable.ConfigYml = yml_config.NewYamlFileFactory()
	variable.ConfigYml.ConfigFileChangeListen()
	variable.ConfigGormYml = variable.ConfigYml.Clone("gorm")
	variable.ConfigGormYml.ConfigFileChangeListen()
	dbMysql, _ := gorminit.InitMysqlConnect()
	variable.GormDBMysql = dbMysql
	fmt.Println("dbMysql = ", dbMysql)

	enforcer, err := initcasbin.InitCasBinEnforcer()
	fmt.Println("enforcer", enforcer)
	fmt.Println("err", err)

	result := enforcer.GetFilteredGroupingPolicy(0, "g_8")
	fmt.Println("result : ", result)
	policies := enforcer.GetFilteredPolicy(0, fmt.Sprintf("g_%d", 8))
	fmt.Println("policies = ", policies)
	for _, policy := range policies {
		result := strings.Split(policy[1], "_")
		fmt.Println("result ", convert.Int64(result[1]))
	}

}
func TestStringSplit(T *testing.T) {
	str := "g_8"
	result := strings.Split(str, "_")
	fmt.Println("result = ", result[1])
}
