package test

import (
	"fmt"
	"os"
	"testing"
	"yooome/global/variable"
	"yooome/model/auth_rule"
	initcasbin "yooome/utils/casbin"
	"yooome/utils/gorminit"
	"yooome/utils/yml_config"
)

func TestAuthRuleDemo(T *testing.T) {
	// 1.检查配置文件是否存在
	if _, err := os.Stat(variable.BasePath + "/config/gorm.yml"); err != nil {
		fmt.Println("配置文件gorm.yml不存在....")
	}
	variable.ConfigYml = yml_config.NewYamlFileFactory()
	variable.ConfigYml.ConfigFileChangeListen()
	variable.ConfigGormYml = variable.ConfigYml.Clone("gorm")
	variable.ConfigGormYml.ConfigFileChangeListen()
	dbMysql, _ := gorminit.InitMysqlConnect()
	variable.GormDBMysql = dbMysql
	fmt.Println("dbMysql = ", dbMysql)

	enforcer, err := initcasbin.InitCasBinEnforcer()
	fmt.Println("enforcer", enforcer)
	fmt.Println("err", err)

	list, err := auth_rule.NewAuthRuleFactory("").GetMenuList()
	if err != nil {
		return
	}
	fmt.Println("Menu list = ", len(list))
	for index, value := range list {
		fmt.Println("list[", index, "] = ", value.Id)
	}
}
