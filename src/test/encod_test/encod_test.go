package encod_test

import (
	"encoding/base64"
	"fmt"
	"testing"
)

func TestEncodeCode(T *testing.T) {
	str := "qwer"
	// cXdlcg==
	encode := base64.StdEncoding.EncodeToString([]byte(str))
	decode, _ := base64.StdEncoding.DecodeString(encode)
	fmt.Println("encode = ", encode)
	fmt.Println("decode = ", string(decode))
}
