package reflect_test

import (
	"fmt"
	"reflect"
	"testing"
)

func TestReflectDemo(T *testing.T) {
	var newReflect reflect.Value
	var types interface{}
	types = map[string]interface{}{
		"uuid":     "123456",
		"username": "zhangSan",
		"password": 123456,
	}
	v, ok := types.(reflect.Value)
	if ok {
		newReflect = v
	} else {
		newReflect = reflect.ValueOf(types)
	}

	fmt.Println("reflect type = ", newReflect)
	fmt.Println("types = ", types)
	reflectKind := newReflect.Kind()
	fmt.Println("reflect kind = ", reflectKind)
	ptr := reflect.Ptr
	fmt.Println("prt = ", ptr)

}

type Persion struct {
	Name string
	Age  int
}

func TestReflect(T *testing.T) {
	p := Persion{
		Name: "zhang san",
		Age:  18,
	}
	res := reflect.TypeOf(p)
	fmt.Println("reflect type = ", res)
	value := reflect.ValueOf(p)
	fmt.Println("reflect value= ", value)
	// 返回的类型
	resKind := res.Kind()
	fmt.Println("resKind = ", resKind)
	valueKind := value.Kind()
	fmt.Println("valueKind = ", valueKind)
	if reflect.Struct == valueKind {
		fmt.Println("reflect struct is ", valueKind)
	}

}
