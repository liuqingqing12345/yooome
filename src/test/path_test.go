package test

import (
	"fmt"
	"log"
	"os"
	"strings"
	"testing"
)

func TestPathDemo(T *testing.T) {
	path, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("项目路径=", path)
	fmt.Println("len(os.Args) = ", len(os.Args))
	fmt.Println("os.Args[1] = ", os.Args[1])
	for i := 0; i < len(os.Args); i++ {
		fmt.Println("os.Args[", i, "] = ", os.Args[i])
	}
	fmt.Println("string.HasPrefix(os.Args[1]) = ", strings.HasPrefix(os.Args[1], "-test"))
	fmt.Println("-------------------------")
	fmt.Println("----- ", strings.Replace(path, `\test`, "", 1))
	BasePath := strings.Replace(strings.Replace(path, `\test`, "", 1), `/test`, "", 1)

	fmt.Println("BasePath = ", BasePath)
}
