package test_test

import (
	"fmt"
	"testing"
	"yooome/utils/connect"
)

func TestRedisDemo(T *testing.T) {
	fmt.Println("test redis....")
	initRedis := connect.InitRedis()
	cacheKey := "password"
	Timeout := 10 * 24 * 60 * 60 * 1000
	cacheValue := "RedisDemo"
	_, err := initRedis.Do("SETEX", cacheKey, Timeout/1000, []byte(cacheValue))
	if err != nil {
		fmt.Println("do cacheKey err : ", err)
	}
}
