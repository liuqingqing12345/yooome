package operation_params

type OperationParams struct {
	Title     string `form:"title" json:"title"`
	OperName  string `form:"operName" json:"operName"`
	Status    string `form:"status" json:"status"`
	BeginTime string `form:"beginTime" json:"beginTime"`
	EndTime   string `form:"endTime" json:"endTime"`
	PageNum   int    `form:"pageNum" json:"pageNum"`
	PageSize  int    `form:"pageSize" json:"pageSize"`
	SortName  string `form:"sortName" json:"sortName"`
	SortOrder string `form:"sortOrder" json:"sortOrder"`
}
