package auth_model

type GetMenuListParams struct {
	MenuName string `form:"menuName" json:"menuName"`
	Status   string `form:"status" json:"status"`
}

type UpdateMenuParams struct {
	MenuId      int    `json:"menuId" form:"menuId"`
	MenuType    int    `json:"menuType" form:"menuType"`
	Pid         int    `json:"parentId" form:"parentId"`
	Name        string `json:"name" form:"name"`
	Title       string `json:"menuName" form:"menuName"`
	Icon        string `json:"icon" form:"icon"`
	Weigh       int    `json:"orderNum" form:"orderNum"`
	Condition   string `json:"condition" form:"condition"`
	Remark      string `json:"remark" form:"remark"`
	Status      int    `json:"status" form:"status"`
	Always_Show int    `json:"visible" form:"visible"`
	Path        string `json:"path" form:"path"`
	Component   string `json:"component" form:"component"`
	IsFrame     int    `json:"isFrame" form:"isFrame"`
}
type AddMenuParams struct {
	MenuId      int    `json:"menuId" form:"menuId"`
	MenuType    int    `json:"menuType" form:"menuType"`
	Pid         int    `json:"parentId" form:"parentId"`
	Name        string `json:"name" form:"name"`
	Title       string `json:"menuName" form:"menuName"`
	Icon        string `json:"icon" form:"icon"`
	Weigh       int    `json:"orderNum" form:"orderNum"`
	Condition   string `json:"condition" form:"condition"`
	Remark      string `json:"remark" form:"remark"`
	Status      int    `json:"status" form:"status"`
	Always_Show int    `json:"visible" form:"visible"`
	Path        string `json:"path" form:"path"`
	Component   string `json:"component" form:"component"`
	IsFrame     int    `json:"isFrame" form:"isFrame"`
}
