package model

import (
	"fmt"
	"gorm.io/gorm"
	"strings"
	"yooome/global/variable"
)

type BaseModel struct {
	*gorm.DB `gorm:"-" json:"-"`
}

func DBConn(sqlType string) *gorm.DB {
	var db *gorm.DB
	sqlType = strings.Trim(sqlType, " ")
	if sqlType == "" {
		sqlType = variable.ConfigGormYml.GetString("GormMysql.UserDBType")
	}
	switch strings.ToLower(sqlType) {
	case "mysql":
		if variable.GormDBMysql == nil {
			fmt.Println("variable GormDBMysql is error ..... ")
		}
		db = variable.GormDBMysql
	default:
		fmt.Println("mysql connect error ..... ")
	}
	return db
}
