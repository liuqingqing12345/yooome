package role_dept_model

import (
	"errors"
	"yooome/model"
	"yooome/utils/convert"
)

type RoleDept struct {
	model.BaseModel `json:"-"`
	RoleId          int `gorm:"role_id" json:"roleId"`
	DeptId          int `gorm:"dept_id" json:"deptId"`
}

// 创建 dict model 实体工厂
func NewRoleDeptModelFactory(sqlType string) *RoleDept {
	return &RoleDept{BaseModel: model.BaseModel{DB: model.DBConn(sqlType)}}
}
func (d *RoleDept) TableName() string {
	return "role_dept"
}
func (d *RoleDept) GetRoleDeptList(roleId string) (list []int, err error) {
	var roleDept []RoleDept
	db := d.DB.Where("role_id = ?", convert.Int64(roleId)).Find(&roleDept)
	if db.RowsAffected == 0 {
		return nil, errors.New("find role dept error")
	} else {
		d := make([]int, len(roleDept))
		for k, v := range roleDept {
			d[k] = v.DeptId
		}
		return d, nil
	}
}
