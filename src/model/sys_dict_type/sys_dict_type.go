package sys_dict_type

// 分页请求参数
type PageReqParams struct {
	PageNum   int    `form:"pageNum" json:"pageNum"`
	PageSize  int    `form:"pageSize" json:"pageSize"`
	BeginTime string `form:"beginTime" json:"beginTime"`
	EndTime   string `form:"endTime" json:"endTime"`
	DictName  string `form:"dictName" json:"dictName"`
	DictType  string `form:"dictType" json:"dictType"`
	Status    string `form:"status" json:"status"`
}

// 添加字典类型
type AddDictType struct {
	DictId   int    `form:"dictId" json:"dictId"`
	DictName string `form:"dictName" json:"dictName"`
	DictType string `form:"dictType" json:"dictType"`
	Remark   string `form:"remark" json:"remark"`
	Status   uint   `form:"status" json:"status"`
}

// 修改字典类型
type UpdateDictType struct {
	DictId   int    `form:"dictId" json:"dictId"`
	DictName string `form:"dictName" json:"dictName"`
	DictType string `form:"dictType" json:"dictType"`
	Remark   string `form:"remark" json:"remark"`
	Status   uint   `form:"status" json:"status"`
}

// 删除字典类型
type DeleteDictType struct {
	DictIds interface{} `form:"dictIds" json:"dictIds"`
}
