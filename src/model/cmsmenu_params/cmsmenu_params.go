package cmsmenu_params

type CmsMenuParams struct {
	Name   string `form:"name" json:"name"`
	Status string `form:"status" json:"status"`
}
type AddMenuParams struct {
	ParentId            int    `form:"parentId" json:"parentId"`
	ModelId             int    `form:"modelId" json:"modelId"`
	Name                string `form:"name" json:"name"`
	Alias               string `form:"alias" json:"alias"`
	CateType            string `form:"cateType" json:"cateType"`
	Description         string `form:"description" json:"description"`
	Thumbnail           string `form:"thumbnail" json:"thumbnail"`
	InputSeoTitle       string `form:"inputSeoTitle" json:"inputSeoTitle"`
	InputSeoKeywords    string `form:"inputSeoKeywords" json:"inputSeoKeywords"`
	InputSeoDescription string `form:"inputSeoDescription" json:"inputSeoDescription"`
	Status              string `form:"status" json:"status"`
	CateAddress         string `form:"cateAddress" json:"cateAddress"`
	CateContent         string `form:"cateContent" json:"cateContent"`
	ListTemplate        string `form:"listTemplate" json:"listTemplate"`
	ContentTemplate     string `form:"contentTemplate" json:"contentTemplate"`
}
type UpdateMenuParams struct {
	Id                  int    `form:"id" json:"id"`
	ParentId            int    `form:"parentId" json:"parentId"`
	ModelId             int    `form:"modelId" json:"modelId"`
	Name                string `form:"name" json:"name"`
	Alias               string `form:"alias" json:"alias"`
	CateType            int    `form:"cateType" json:"cateType"`
	Description         string `form:"description" json:"description"`
	Thumbnail           string `form:"thumbnail" json:"thumbnail"`
	InputSeoTitle       string `form:"inputSeoTitle" json:"inputSeoTitle"`
	InputSeoKeywords    string `form:"inputSeoKeywords" json:"inputSeoKeywords"`
	InputSeoDescription string `form:"inputSeoDescription" json:"inputSeoDescription"`
	Status              int    `form:"status" json:"status"`
	CateAddress         string `form:"cateAddress" json:"cateAddress"`
	CateContent         string `form:"cateContent" json:"cateContent"`
	ListTemplate        string `form:"listTemplate" json:"listTemplate"`
	ContentTemplate     string `form:"contentTemplate" json:"contentTemplate"`
}
type QueryNewsParams struct {
	PageSize int    `form:"pageSize" json:"pageSize"`
	PageNum  int    `form:"pageNum" json:"pageNum"`
	KeyWords string `form:"keywords" json:"keywords"`
	CateId   []int  `form:"cateId" json:"cateId"`
}
