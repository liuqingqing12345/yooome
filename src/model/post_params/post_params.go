package post_params

import "time"

type SearchPostParams struct {
	PageNum  int    `form:"pageNum" json:"pageNum"`
	PageSize int    `form:"pageSize" json:"pageSize"`
	PostCode string `form:"postCode" json:"postCode"`
	PostName string `form:"postName" json:"postName"`
	Status   string `form:"status" json:"status"`
}

// 添加岗位
type AddPostParams struct {
	PostCode   string    `form:"postCode" json:"postCode"`
	PostName   string    `form:"postName" json:"postName"`
	PostSort   int       `form:"postSort" json:"postSort"`
	Status     string    `form:"status" json:"status"`
	Remark     string    `form:"remark" json:"remark"`
	CreateBy   string    `form:"-" json:"-"`
	CreateTime time.Time `form:"-" json:"-"`
}

// 添加岗位
type UpdatePostParams struct {
	PostId     int       `form:"postId" json:"postId"`
	PostCode   string    `form:"postCode" json:"postCode"`
	PostName   string    `form:"postName" json:"postName"`
	PostSort   int       `form:"postSort" json:"postSort"`
	Status     string    `form:"status" json:"status"`
	Remark     string    `form:"remark" json:"remark"`
	UpdateBy   string    `form:"-" json:"-"`
	UpdateTime time.Time `form:"-" json:"-"`
}
