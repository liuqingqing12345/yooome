package model

import (
	"errors"
	"fmt"
	"yooome/global/variable"
	"yooome/model"
	"yooome/model/user_params"
	"yooome/utils/convert"
	"yooome/utils/tools"
	"yooome/utils/ytime"
)

type UserModel struct {
	model.BaseModel `json:"-"`
	Id              int64  `gorm:"id" json:"id"`
	CreateTime      string `gorm:"create_time" json:"create_time"`
	UpdateTime      string `gorm:"update_time" json:"update_time"`
	Bas64           string `form:"bas64" json:"bas64"`
	UserName        string `form:"username" json:"username" gorm:"user_name"`
	UserPassword    string `form:"password" json:"password" gorm:"user_password"`
	Mobile          string `form:"mobile" json:"mobile" gorm:"mobile"`
	UserStatus      string `json:"status" gorm:"user_status"`
	UserNickname    string `json:"nickname" gorm:"user_nickname"`
	Birthday        int8   `json:"birthday" gorm:"birthday"`
	UserEmail       string `json:"email" gorm:"user_email"`
	Sex             int8   `json:"sex" gorm:"sex"`
	Avatar          string `json:"avatar" gorm:"avatar"`
	LastLoginTime   int64  `json:"logintime" gorm:"last_login_time"`
	LastLoginIp     string `json:"ip" gorm:"last_login_ip"`
	DeptId          int16  `json:"deptid" gorm:"dept_id"`
	Remark          string `json:"remark" gorm:"remark"`
	IsAdmin         int8   `json:"isadmin" gorm:"is_admin"`
}

func NewUserFactory(sqlType string) *UserModel {
	return &UserModel{
		BaseModel: model.BaseModel{DB: model.DBConn(sqlType)},
	}
}
func (u *UserModel) TableName() string {
	return "tb_user"
}

// 用户登录
func (u *UserModel) Login(username string, pass string) *UserModel {
	sql := "select id, user_name, mobile, user_nickname, birthday, user_status, create_time, user_password, user_email, sex, " +
		"avatar, last_login_time, last_login_ip, dept_id, remark, is_admin from tb_user where  user_name=?  limit 1"
	result := u.Raw(sql, username).First(u)
	if result.Error == nil {
		// 账号密码验证成功
		//if len(u.PassWord) > 0 && (u.PassWord == md5_encrypt.Base64Md5(pass)) {
		return u
		//}
	} else {
		// variable.ZapLog.Error("根据账号查询单条记录出错:", zap.Error(result.Error))
		fmt.Println("根据账号查询单挑记录出错")
		return nil
	}
}
func (u *UserModel) GetLoginAdminInfo() {

}
func (u *UserModel) GetUserById(id int64) (*UserModel, error) {
	sql := "select id, user_name, mobile, user_nickname, birthday, user_status, create_time, user_password, user_email, sex, " +
		"avatar, last_login_time, last_login_ip, dept_id, remark, is_admin from tb_user where  id=? "
	result := u.Raw(sql, id).First(u)
	if result.Error == nil {
		// 根据id获取用户的信息
		return u, nil
	} else {
		fmt.Println("未能获取到用户信息")
		return nil, errors.New("获取用户的信息error")
	}
}
func (u *UserModel) GetUserList(userParams *user_params.GetUserParams) (total, page int, list []*UserModel, err error) {
	db := u.DB.Find(&list)
	if userParams != nil {
		if userParams.UserName != "" {
			db = db.Where("user_name like ? or user_nickname like ?", "%"+userParams.UserName+"%", "%"+userParams.UserName+"%")
		}
		if len(userParams.DeptId) != 0 {
			db = db.Where("dept_id in (?)", userParams.DeptId)
		}
		if userParams.PhoneNumber != "" {
			db = db.Where("mobile like ?", "%"+userParams.PhoneNumber+"%")
		}
		if userParams.BeginTime != "" {
			tm, _ := ytime.StrToTime(userParams.BeginTime)
			db = db.Where("create_time >= ?", tm.Timestamp())
		}
		if userParams.EndTime != "" {
			tm, _ := ytime.StrToTime(userParams.EndTime)
			db = db.Where("end_time <= ?", tm.Timestamp())
		}
	}
	total = len(list)
	page = userParams.PageNum
	if userParams.PageNum > 0 && userParams.PageSize > 0 {
		res := (userParams.PageNum - 1) * userParams.PageSize
		db.Limit(userParams.PageSize).Offset(res).Order("id asc").Find(&list)
	}
	return total, page, list, nil

}
func (u *UserModel) AddUser(params *user_params.AddUserParams) int64 {
	var resUser UserModel
	//   密码加密
	publicKey := variable.ConfigYml.GetString("adminInfo.encryptKey")
	// 用户密码进行加密

	db := u.DB.Where("user_name=?", params.UserName)
	if db.RowsAffected > 0 {
		return -1
	}
	db = u.DB.Where("mobile=?", params.PhoneNumber)
	if db.RowsAffected > 0 {
		return -1
	}
	// 保存管理信息
	password := tools.EncryptCBC(params.PassWord, publicKey)
	userName := params.UserName
	deptId := params.DeptId
	status := params.Status
	createTime := ytime.Now().Timestamp()
	mobile := params.PhoneNumber
	sex := params.Sex
	email := params.Email
	nickName := params.NickName
	remark := params.Remark
	isAdmin := params.IsAdmin

	sql := "insert into tb_user (user_name,mobile,user_nickname,create_time,user_password,user_status,user_email,sex,dept_id,remark,is_admin)" +
		"values (?,?,?,?,?,?,?,?,?,?,?)"
	res := u.DB.Exec(sql, userName, mobile, nickName, createTime, password, status, email, sex, deptId, remark, isAdmin)
	if res.RowsAffected > 0 {
		sqlStr := "select * from tb_user where user_name = ?"
		u.DB.Raw(sqlStr, userName).Scan(&resUser)
		return resUser.Id
	} else {
		return -1
	}
}

func (u *UserModel) AddUserPost(roleIds []int, userId int64) bool {
	// 删除旧的岗位信息
	sql := "delete from user_post where user_id = ?"
	db := u.DB.Exec(sql, userId)
	// 添加岗位信息
	for _, v := range roleIds {
		sqlStr := "insert into user_post (post_id,user_id) values (?,?)"
		db = u.DB.Exec(sqlStr, v, userId)
	}

	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}

// 获取用户信息
func (u *UserModel) GetUserInfoById(id string) (userInfo *UserModel) {
	// sql := "select * from tb_user where id = ?"
	db := u.DB.Where("id = ?", convert.Int64(id)).Find(&userInfo)
	if db.RowsAffected > 0 {
		return userInfo
	} else {
		return nil
	}
}
func (u *UserModel) UpdateUserInfo(params *user_params.UpdateUserParams) bool {
	// 查询手机号码是否已经存在
	db := u.DB.Where("id != ? and mobile = ?", convert.Int64(params.UserId), params.PhoneNumber)
	if db.RowsAffected > 0 {
		return false
	}
	// 保存管理员信息
	userName := params.UserName
	mobile := params.PhoneNumber
	deptId := params.DeptId
	status := params.Status
	sex := params.Sex
	email := params.Email
	nickName := params.NickName
	remark := params.Remark
	isAdmin := params.IsAdmin

	sql := "update tb_user set user_name = ?,mobile = ?,user_nickname = ?,user_status = ?,user_email = ?,sex = ?,dept_id = ?,remark = ?,is_admin = ?" +
		" where id = ?"
	db = u.DB.Exec(sql, userName, mobile, nickName, status, email, sex, deptId, remark, isAdmin, convert.Int64(params.UserId))
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
func (u *UserModel) UpdateUserRole(ids []int, userId int) bool {
	enforcer := variable.Enforcer
	// 删除用户旧角色信息
	enforcer.RemoveFilteredGroupingPolicy(0, fmt.Sprintf("u_%d", userId))
	for _, v := range ids {
		_, err := enforcer.AddGroupingPolicy(fmt.Sprintf("u_%d", convert.Int64(userId)), fmt.Sprintf("g_%d", v))
		if err != nil {
			return false
		}
	}
	return true
}
func (u *UserModel) DelUser(userIds []int64) bool {
	sql := "delete from tb_user where id in (?)"
	db := u.DB.Exec(sql, userIds)
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
func (u *UserModel) DelUserPost(userIds []int64) bool {
	sql := "delete from user_post where user_id in (?)"
	db := u.DB.Exec(sql, userIds)
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
func (u *UserModel) ResetUserPwd(params *user_params.ResetPwdRew) bool {
	// 密码进行加密
	//   密码加密
	publicKey := variable.ConfigYml.GetString("adminInfo.encryptKey")
	password := tools.EncryptCBC(params.Password, publicKey)
	sql := "update tb_user set user_password = ? where id =?"
	db := u.DB.Exec(sql, password, params.Id)
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
