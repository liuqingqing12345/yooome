package job_params

type QueryJobParams struct {
	JobName  string `form:"jobName" json:"jobName"`
	JobGroup string `form:"jonGroup" json:"jobGroup"`
	Status   string `form:"status" json:"status"`
	PageSize int    `form:"pageSize" json:"pageSize"`
	PageNum  int    `form:"pageNum" json:"pageNum"`
}
type AddJobParams struct {
	JobName        string `form:"jobName" json:"jobName"`
	JobParams      string `form:"jobParams" json:"jobParams"`
	JobGroup       string `form:"jobGroup" json:"jobGroup"`
	InvokeTarget   string `form:"invokeTarget" json:"invokeTarget"`
	CronExpression string `form:"cronExpression" json:"cronExpression"`
	MisfirePolicy  string `form:"misfirePolicy" json:"misfirePolicy"`
	Concurrent     int    `form:"concurrent" json:"concurrent"`
	Status         string `form:"status" json:"status"`
	Remark         string `form:"remark" json:"remark"`
}
type UpdateJobParams struct {
	JobId          int    `form:"jobId" json:"jobId"`
	JobName        string `form:"jobName" json:"jobName"`
	JobParams      string `form:"jobParams" json:"jobParams"`
	JobGroup       string `form:"jobGroup" json:"jobGroup"`
	InvokeTarget   string `form:"invokeTarget" json:"invokeTarget"`
	CronExpression string `form:"cronExpression" json:"cronExpression"`
	MisfirePolicy  string `form:"misfirePolicy" json:"misfirePolicy"`
	Concurrent     int    `form:"concurrent" json:"concurrent"`
	Status         string `form:"status" json:"status"`
	Remark         string `form:"remark" json:"remark"`
}
