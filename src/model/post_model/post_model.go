package post_model

import (
	"errors"
	"time"
	"yooome/model"
	"yooome/model/post_params"
	"yooome/utils/convert"
)

type PostModel struct {
	model.BaseModel `json:"-"`
	PostId          int        `gorm:"post_id" json:"postId"`
	PostCode        string     `gorm:"post_code" json:"postCode"`
	PostName        string     `gorm:"post_name" json:"postName"`
	PostSort        int        `gorm:"post_sort" json:"postSort"`
	Status          string     `gorm:"status" json:"status"`
	CreateBy        string     `gorm:"create_by" json:"createBy"`
	UpdateBY        string     `gorm:"update_by" json:"updateBY"`
	CreateTime      *time.Time `gorm:"create_time" json:"createTime"`
	UpdateTime      *time.Time `gorm:"update_time" json:"updateTime"`
	Remark          string     `gorm:"remark" json:"remark"`
}

// 创建 dict model 实体工厂
func NewPostModelFactory(sqlType string) *PostModel {
	return &PostModel{BaseModel: model.BaseModel{DB: model.DBConn(sqlType)}}
}
func (p *PostModel) TableName() string {
	return "sys_post"
}
func (p *PostModel) ListPost(params *post_params.SearchPostParams) (total, page int, list []*PostModel, err error) {
	db := p.DB.Find(&list)
	if params.PostCode != "" {
		db = db.Where("post_code like ?", "%"+params.PostCode+"%").Find(&list)
	}
	if params.PostName != "" {
		db = db.Where("post_Name like ?", "%"+params.PostName+"%").Find(&list)
	}
	if params.Status != "" {
		db = db.Where("status = ?", params.Status).Find(&list)
	}
	total = len(list)
	if params.PageNum == 0 {
		params.PageNum = 1
	}
	page = params.PageNum
	if params.PageNum > 0 && params.PageSize > 0 {
		res := (params.PageNum - 1) * params.PageSize
		db.Limit(params.PageSize).Offset(res).Order("post_id asc").Find(&list)
	}
	return total, page, list, err
}

// 添加岗位工人
func (p *PostModel) AddPost(params *post_params.AddPostParams) bool {
	postCode := params.PostCode
	postSort := params.PostSort
	postName := params.PostName
	status := params.Status
	remark := params.Remark
	createBy := params.CreateBy
	createTime := params.CreateTime
	sql := "insert into sys_post (post_code,post_sort,post_name,status,remark,create_by,create_time) values(?,?,?,?,?,?,?)"
	db := p.DB.Exec(sql, postCode, postSort, postName, status, remark, createBy, createTime)
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
func (p *PostModel) GetPostById(id string) (post *PostModel, err error) {
	db := p.DB.Where("post_id = ?", convert.Int64(id)).Find(&post)
	if db.RowsAffected > 0 {
		return post, nil
	} else {
		return nil, errors.New("get post by id error")
	}
}
func (p *PostModel) UpdatePost(params *post_params.UpdatePostParams) bool {
	postId := params.PostId
	postCode := params.PostCode
	postSort := params.PostSort
	postName := params.PostName
	status := params.Status
	remark := params.Remark
	updateBy := params.UpdateBy
	updateTime := params.UpdateTime
	sql := "update sys_post set post_code = ?,post_sort = ?,post_name = ?,status = ?,remark = ?,update_by = ?,update_time = ? where post_id = ?"
	db := p.DB.Exec(sql, postCode, postSort, postName, status, remark, updateBy, updateTime, postId)
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
func (p *PostModel) DeletePost(ids []int) bool {
	sql := "delete from sys_post where post_id in (?)"
	db := p.DB.Exec(sql, ids)
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
func (p *PostModel) GetListPost() (list []*PostModel, err error) {
	sql := "select * from sys_post order by post_id asc"
	db := p.DB.Exec(sql).Find(&list)
	if db.RowsAffected > 0 {
		return list, nil
	} else {
		return nil, errors.New("get list post")
	}
}
func (p *PostModel) GetStatusPost() (list []*PostModel, err error) {
	sql := "select * from sys_post where status = ? order by post_id asc"
	db := p.DB.Exec(sql, 1).Find(&list)
	if db.RowsAffected > 0 {
		return list, nil
	} else {
		return nil, errors.New("get list status post")
	}
}
func (p *PostModel) GetUserPostById(id string) (postIds []int64, err error) {
	sql := "select post_id from user_post where user_id = ?"
	db := p.DB.Raw(sql, convert.Int64(id)).Find(&postIds)
	if db.RowsAffected > 0 {
		return postIds, nil
	} else {
		return nil, errors.New("get user post by id error")
	}
}
