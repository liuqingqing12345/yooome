package monitor_params

type OnLineParams struct {
	PageSize int    `form:"pageSize" json:"pageSize"`
	PageNum  int    `form:"pageNum" json:"pageNum"`
	UserName string `form:"userName" json:"userName"`
	IpAddr   string `form:"ipaddr" json:"ipaddr"`
}
