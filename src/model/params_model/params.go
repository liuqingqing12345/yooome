package params_model

import (
	"yooome/model"
	"yooome/model/sys_config_params"
	"yooome/utils/convert"
	"yooome/utils/ytime"
)

type ConfigParams struct {
	model.BaseModel `json:"-"`
	ConfigId        uint   `gorm:"config_id" json:"config_id"`
	ConfigName      string `gorm:"config_name" json:"config_name"`
	ConfigKey       string `gorm:"config_key" json:"config_key"`
	ConfigValue     string `gorm:"config_value" json:"config_value"`
	ConfigType      string `gorm:"config_type" json:"config_type"`
	CreateBy        uint   `gorm:"create_by" json:"create_by"`
	CreateTime      uint   `gorm:"create_time" json:"create_time"`
	UpdateBy        uint   `gorm:"update_by" json:"update_by"`
	UpdateTime      uint   `gorm:"update_time" json:"update_time"`
	Remark          string `gorm:"remark" json:"remark"`
}

func NewConfigParamsFactory(sqlType string) *ConfigParams {
	return &ConfigParams{BaseModel: model.BaseModel{DB: model.DBConn(sqlType)}}
}
func (p *ConfigParams) TableName() string {
	return "sys_config"
}

// 获取参数列表
func (p *ConfigParams) GetParamsList(params *sys_config_params.ParamsPageReq) (total int, page int, list []*ConfigParams, err error) {
	db := p.DB.Find(&list)
	if params != nil {
		if params.ConfigName != "" {
			db = db.Where("config_name like ?", "%"+params.ConfigName+"%").Find(&list)
		}
		if params.ConfigType != "" {
			db = db.Where("config_type = ?", convert.Int64(params.ConfigType)).Find(&list)
		}
		if params.ConfigKey != "" {
			db = db.Where("config_key = ?", params.ConfigKey).Find(&list)
		}
		if params.BeginTime != "" {
			t, _ := ytime.StrToTime(params.BeginTime)
			db = db.Where("create_time >= ?", t.Timestamp()).Find(&list)
		}
		if params.EndTime != "" {
			t, _ := ytime.StrToTime(params.EndTime)
			db = db.Where("create_time <= ?", t.Timestamp()).Find(&list)
		}
	}
	total = len(list)
	if params.PageNum == 0 {
		params.PageNum = 1
	}
	if params.PageNum > 0 && params.PageSize > 0 {
		res := (params.PageNum - 1) * params.PageSize
		db.Limit(params.PageSize).Offset(res).Order("config_id asc").Find(&list)
	}
	return total, params.PageNum, list, nil
}

// 检查configKey 是否唯一
func (p *ConfigParams) CheckConfigKeyUnique(configKey string) bool {
	var params *ConfigParams
	if configKey == "" {
		return false
	}
	res := p.DB.Where("config_key = ?", configKey).Find(&params)
	if res.RowsAffected > 0 {
		return false
	} else {
		return true
	}
}

// 添加参数数据
func (p *ConfigParams) AddConfigParams(params *sys_config_params.AddParamsReq, userId int64) int64 {
	configName := params.ConfigName
	configKey := params.ConfigKey
	configValue := params.ConfigValue
	configType := params.ConfigType
	createBy := userId
	createTime := convert.Uint64(ytime.Timestamp())
	remark := params.Remark
	sql := "insert into sys_config (config_name,config_key,config_value,config_type,create_by,create_time,remark) values (?,?,?,?,?,?,?)"
	res := p.DB.Exec(sql, configName, configKey, configValue, configType, createBy, createTime, remark)
	if res.RowsAffected > 0 {
		return res.RowsAffected
	} else {
		return res.RowsAffected
	}
}

// 更新参数数据
func (p *ConfigParams) UpdateConfig(params *sys_config_params.UpdParamsReq, userId int64) int64 {
	configId := convert.Int64(params.ConfigId)
	configName := params.ConfigName
	configKey := params.ConfigKey
	configValue := params.ConfigValue
	configType := params.ConfigType
	updateBy := userId
	updateTime := convert.Uint64(ytime.Timestamp())
	remark := params.Remark
	sql := "update sys_config set config_name = ?, config_key = ?,config_value = ?,config_type = ?,update_by = ?,update_time = ?,remark = ? where config_id = ?"
	res := p.DB.Exec(sql, configName, configKey, configValue, configType, updateBy, updateTime, remark, configId)
	if res.RowsAffected > 0 {
		return res.RowsAffected
	} else {
		return res.RowsAffected
	}
}

// 根据id 获取参数信息
func (p *ConfigParams) GetConfigDetailById(id uint64) (param *ConfigParams) {
	// sql := "select * from sys_config where config_id = ?"
	res := p.DB.Where("config_id = ?", id).Find(&param)
	if res.RowsAffected > 0 {
		return param
	} else {
		return nil
	}
}

// 根据id删除 参数信息
func (p *ConfigParams) DelConfig(configIds []int) bool {
	sql := "delete from sys_config where config_id in (?)"
	res := p.DB.Exec(sql, configIds)
	if res.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
