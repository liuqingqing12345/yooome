package user_params

type GetUserParams struct {
	DeptId      string  `form:"deptId" json:"deptId"`
	PageNum     int     `form:"pageNum" json:"pageNum"`
	PageSize    int     `form:"pageSize" json:"pageSize"`
	UserName    string  `form:"userName" json:"userName"`
	PhoneNumber string  `form:"phoneNumber" json:"phoneNumber"`
	Status      string  `form:"status" json:"status"`
	DeptIds     []int64 `form:"-" json:"-"`
	BeginTime   string  `form:"beginTime" json:"beginTime"`
	EndTime     string  `form:"endTime" json:"endTime"`
}
type AddUserParams struct {
	DeptId      int    `form:"deptId" json:"deptId"`
	Email       string `form:"email" json:"email"`
	IsAdmin     int    `form:"isadmin" json:"isadmin"`
	NickName    string `form:"nickName" json:"nickName"`
	PassWord    string `form:"passWord" json:"passWord"`
	PhoneNumber string `form:"phoneNumber" json:"phoneNumber"`
	PostIds     []int  `form:"postIds" json:"postIds"`
	Remark      string `form:"remark" json:"remark"`
	RoleIds     []int  `form:"roleIds" json:"roleIds"`
	Sex         string `form:"sex" json:"sex"`
	Status      string `form:"status" json:"status"`
	UserId      string `form:"userId" json:"userId"`
	UserName    string `form:"userName" json:"userName"`
}

// 添加用户参数
type UpdateUserParams struct {
	UserId      int    `form:"userId" json:"userId"`
	DeptId      int    `form:"deptId" json:"deptId"`
	UserName    string `form:"userName" json:"userName"`
	NickName    string `form:"nickName" json:"nickName"`
	PassWord    string `form:"passWord" json:"passWord"`
	Email       string `form:"email" json:"email"`
	Sex         string `form:"sex" json:"sex"`
	Status      string `form:"status" json:"status"`
	Remark      string `form:"remark" json:"remark"`
	PostIds     []int  `form:"postIds" json:"postIds"`
	RoleIds     []int  `form:"roleIds" json:"roleIds"`
	IsAdmin     int    `form:"isadmin" json:"isadmin"`
	PhoneNumber string `form:"phoneNumber" json:"phoneNumber"`
}
type ResetPwdRew struct {
	Id       int64  `form:"userId" json:"userId"`
	Password string `form:"password" json:"password"`
}
