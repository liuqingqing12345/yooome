package online_model

import (
	"yooome/model"
	"yooome/model/monitor_params"
)

type OnlineModel struct {
	model.BaseModel `json:"-"`
	Id              int64  `gorm:"id" json:"id"`
	Uuid            string `gorm:"uuid" json:"uuid"`
	Token           string `gorm:"token" json:"token"`
	CreateTime      int64  `gorm:"create_time" json:"createTime"`
	UserName        string `gorm:"userName" json:"userName"`
	Os              string `gorm:"os" json:"os"`
	Ip              string `gorm:"ip" json:"ip"`
	Explorer        string `gorm:"explorer" json:"explorer"`
}

func NewOnlineModelFactory(sqlType string) *OnlineModel {
	return &OnlineModel{BaseModel: model.BaseModel{DB: model.DBConn(sqlType)}}
}
func (o *OnlineModel) TableName() string {
	return "user_online"
}
func (o *OnlineModel) GetOnlineList(params *monitor_params.OnLineParams) (total, page int, online []*OnlineModel, err error) {
	userName := params.UserName
	ipaddr := params.IpAddr
	db := o.DB.Find(&online)
	if userName != "" {
		db = db.Where("user_name like ?", userName).Find(&online)
	}
	if ipaddr != "" {
		db = db.Where("ipaddr like ?", ipaddr).Find(&online)
	}
	total = len(online)
	if params.PageNum == 0 {
		params.PageNum = 1
	}
	if params.PageSize == 0 {
		params.PageSize = 10
	}
	if params.PageNum > 0 && params.PageSize > 0 {
		res := (params.PageNum - 1) * params.PageSize
		db.Limit(params.PageSize).Offset(res).Order("create_time desc").Find(&online)
	}
	return total, params.PageSize, online, nil
}
