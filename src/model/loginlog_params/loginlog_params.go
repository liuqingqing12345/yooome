package loginlog_params

// 查询列表请求参数
type GetLoginLogParams struct {
	LoginName string `form:"loginName" json:"loginName"`
	Status    string `form:"status" json:"status"`
	Ipaddr    string `form:"ipaddr" json:"ipaddr"`
	BeginTime string `form:"beginTime" json:"beginTime"`
	EndTime   string `form:"endTime" json:"endTime"`
	PageNum   int    `form:"pageNum" json:"pageNum"`
	PageSize  int    `form:"pageSize" json:"pageSize"`
	SortName  string `form:"sortName" json:"sortName"`
	SortOrder string `form:"sortOrder" json:"sortOder"`
}
