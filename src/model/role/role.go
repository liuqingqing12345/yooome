package role

import (
	"errors"
	"fmt"
	"github.com/sirupsen/logrus"
	"strings"
	"yooome/global/variable"
	"yooome/model"
	"yooome/model/req_role_params"
	"yooome/model/role_params"
	"yooome/utils/convert"
	"yooome/utils/ytime"
)

type Role struct {
	model.BaseModel `json:"-"`
	Id              int64   `gorm:"id" json:"id"`
	CreateTime      string  `gorm:"create_time" json:"create_time"`
	UpdateTime      string  `gorm:"update_time" json:"update_time"`
	Status          int8    `json:"status" gorm:"status"`
	ListOrder       float64 `json:"listorder" gorm:"list_order"`
	Name            string  `json:"name" gorm:"name"`
	Remark          string  `json:"remark" gorm:"remark"`
	DataScope       int8    `json:"datascope" gorm:"data_scope"`
}

func NewRoleFactory(sqlType string) *Role {
	return &Role{BaseModel: model.BaseModel{DB: model.DBConn(sqlType)}}
}
func (r *Role) TableName() string {
	return "role"
}
func (r *Role) GetRoleList() (list []*Role, err error) {
	sql := "select * from role"
	// result := r.Raw(sql, id).First(r)
	r.Raw(sql).Find(&list)
	if list == nil {
		logrus.Error("role list empty,please check role")
		return nil, errors.New("get role list empty")
	}
	return list, nil
}

// 获取所有的菜单
func (r *Role) GetMenuList() (list []*Role, err error) {
	// TODO 从缓存中获取菜单信息

	// 从数据库中获取菜单信息
	// 数据库查询语言
	//sql := "select * from "
	//r.Raw().Find(&list)
	// TODO 将数据库中获取的菜单信息进行缓存
	return list, err
}
func (r *Role) GetMenuListByParams(roleParams *req_role_params.ReqRoleParams) (total, page int, list []*Role, err error) {
	db := r.DB.Find(&list)
	if roleParams != nil {
		if roleParams.RoleName != "" {
			db = db.Where("name like ?", "%"+roleParams.RoleName+"%").Find(&list)
		}
		if roleParams.Status != "" {
			db = db.Where("status = ?", convert.Int64(roleParams.Status)).Find(&list)
		}
		if roleParams.BeginTime != "" {
			t, _ := ytime.StrToTime(roleParams.BeginTime)
			db = db.Where("create_time >= ?", t.Timestamp()).Find(&list)
		}
		if roleParams.EndTime != "" {
			t, _ := ytime.StrToTime(roleParams.EndTime)
			db = db.Where("end_time >= ?", t.Timestamp()).Find(&list)
		}
	}
	total = len(list)
	if roleParams.PageNum == 0 {
		roleParams.PageNum = 1
	}
	if roleParams.PageNum > 0 && roleParams.PageSize > 0 {
		res := (roleParams.PageNum - 1) * roleParams.PageSize
		db.Limit(roleParams.PageSize).Offset(res).Order("id asc").Find(&list)
	}
	return total, roleParams.PageNum, list, nil
}

// 新增 角色
func (r *Role) AddRole(roleParams *req_role_params.AddRoleParams) bool {
	name := roleParams.Name
	status := roleParams.Status
	remark := roleParams.Remark
	listOrder := roleParams.ListOrder
	createTime := convert.Uint64(ytime.Timestamp())
	updateTime := convert.Uint64(ytime.Timestamp())
	sql := "insert into role (name,status,remark,list_order,create_time,update_time) values (?,?,?,?,?,?)"
	db := r.DB.Exec(sql, name, status, remark, listOrder, createTime, updateTime)
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
func (r *Role) GetRoleById(id string) (role *Role) {
	db := r.DB.Where("id = ?", convert.Int64(id)).Find(&role)
	if db.RowsAffected > 0 {
		return role
	} else {
		return nil
	}
}
func (r *Role) UpdateRole(roleParams *req_role_params.UpdateRoleParams) bool {
	sql := "update role set status = ?,name = ?,list_order = ?,remark = ? where id = ?"
	db := r.DB.Exec(sql, convert.Int64(roleParams.Status), roleParams.Name, roleParams.ListOrder, roleParams.Remark, roleParams.Id)
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}

// 删除 角色
func (r *Role) DeleteRole(ids []int) bool {
	sql := "delete from role where id in (?)"
	db := r.DB.Exec(sql, ids)
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
func (r *Role) RoleDataScope(roleParams *role_params.RoleParams) bool {
	db := r.DB.Begin()
	sql := "update role set data_scope = ? where id = ?"
	db.Exec(sql, convert.Int64(roleParams.DataScope), roleParams.RoleId)
	if convert.Int64(roleParams.DataScope) == 2 {
		roleSql := "delete from role_dept where role_id = ?"
		db.Exec(roleSql, roleParams.RoleId)
		for _, deptId := range roleParams.DeptIds {
			deptSql := "insert into role_dept (dept_id,role_id) values (?,?)"
			db = db.Exec(deptSql, deptId, roleParams.RoleId)
		}
	}
	db.Commit()
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
func (r *Role) AddUserRole(roleIds []int, userId int64) bool {
	enforcer := variable.Enforcer
	for _, v := range roleIds {
		_, err := enforcer.AddGroupingPolicy(fmt.Sprintf("u_%d", userId), fmt.Sprintf("g_%d", v))
		if err != nil {
			return false
		}
	}
	return true
}
func (r *Role) GetAdminRoleIds(userId string) (roleIds []int64, err error) {
	enforcer := variable.Enforcer
	// 查询关联角色规则
	groupPolicy := enforcer.GetFilteredGroupingPolicy(0, fmt.Sprintf("u_%d", convert.Int64(userId)))
	if len(groupPolicy) > 0 {
		roleIds = make([]int64, len(groupPolicy))
		// 得到角色 id 的切片
		for k, v := range groupPolicy {
			roleIds[k] = convert.Int64(strings.Split(v[1], "_")[1])
		}
	}
	return roleIds, nil
}
