package sys_dict_data

import (
	"yooome/model"
)

// Entity struct of the table sys_dict_data
type DictData struct {
	model.BaseModel `json:"-"`
	DictCode        int64  `gorm:"dict_code"   json:"dict_code"`
	DictSort        int64  `gorm:"dict_sort"   json:"dict_sort"`
	DictLabel       string `gorm:"dict_label"  json:"dict_label"`
	DictValue       string `gorm:"dict_value"  json:"dict_value"`
	DictType        string `gorm:"dict_type"   json:"dict_type"`
	CssClass        string `gorm:"css_class"   json:"css_class"`
	ListClass       string `gorm:"list_class"  json:"list_class"`
	IsDefault       int    `gorm:"is_default"  json:"is_default"`
	Status          int    `gorm:"status"      json:"status"`
	CreateBy        uint64 `gorm:"create_by"   json:"create_by"`
	CreateTime      uint64 `gorm:"create_time" json:"create_time"`
	UpdateBy        uint64 `gorm:"update_by"   json:"update_by"`
	UpdateTime      uint64 `gorm:"update_time" json:"update_time"`
	Remark          string `gorm:"remark"      json:"remark"`
}

// 创建 DictData 实体
func NewDictDataFactory(sqlType string) *DictData {
	return &DictData{BaseModel: model.BaseModel{DB: model.DBConn(sqlType)}}
}
func (d *DictData) TableName() string {
	return "sys_dict_data"
}

// 获取字典数据
func (d *DictData) GetStatusByDictType(dictType string) (list []*DictData) {
	if dictType == "" {
		return nil
	}
	d.DB.Where("dict_type = ?", dictType).Find(&list).Order("dict_sort asc,dict_code asc")
	return list
}
