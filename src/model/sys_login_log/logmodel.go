package sys_login_log

// LoginLog  is the golang struct for table sys_login_log
type LoginLog struct {
	InfoId     int64  `json:"info_id"`				// 访问ID
	LoginName  string `json:"login_name"`			// 登录账号
	Ipaddr     string `json:"ipaddr"`				// 登录IP地址
	LoginLocal string `json:"login_local"`			// 登录地点
	Browser    string `json:"browser"`				// 浏览器类型
	Os         string `json:"os"`					// 操作系统
	Status     string `json:"status"`				// 登录状态(0:成功，1:失败）
	Msg        string `json:"msg"`					// 提示消息
	LoginTime  int64  `json:"login_time"`			// 访问时间
	Module     string `json:"module"`				// 登录模块
}

func (l *LoginLog)  Insert() (){
	
}