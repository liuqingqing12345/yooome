package valid

import (
	"github.com/gin-gonic/gin"
	authmenu "yooome/controller/auth"
	"yooome/controller/cmsmenuList"
	"yooome/controller/common"
	"yooome/controller/cron"
	"yooome/controller/department"
	configdict "yooome/controller/dict"
	"yooome/controller/monitor"
	"yooome/controller/operationLog"
	configparams "yooome/controller/params"
	"yooome/controller/post"
	"yooome/controller/role"
	systemInfo "yooome/controller/systeminfo"
	"yooome/controller/usermanagement"
	configwebset "yooome/controller/webset"
	validFormFactory "yooome/core/factory"
	"yooome/global/contsts"
)

func CaptchaRouters(validRouter *gin.RouterGroup) {
	{
		validRouter.GET("/get", (&common.Captcha{}).GetImg)
		// 可以去除这个 验证，直接在login中进行验证
		validRouter.POST("/validCode", (&common.Captcha{}).ValidCaptcha)
	}
}
func AdminRouters(validRouter *gin.RouterGroup) {
	{
		auth := validRouter.Group("/users")
		{
			auth.POST("/login", validFormFactory.ValidFormFactory(contsts.ValidatorPrefix+"UserLogin"))
		}
	}
}
func SysLoginGroup(validRouter *gin.RouterGroup) {
	{
		validRouter.POST("/login", validFormFactory.ValidFormFactory(contsts.ValidatorPrefix+"UserLogin"))
	}
}
func SystemGroup(validRouter *gin.RouterGroup) {
	{
		index := validRouter.Group("/index")
		{
			index.GET("/getInfo", (&systemInfo.SystemInfo{}).GetInfo)
			index.GET("/getRouters", (&systemInfo.SystemInfo{}).GetRouters)
		}
		dict := validRouter.Group("/config/dict")
		{
			dict.GET("/list", (&configdict.ConfigDict{}).GetDictList)
			dict.POST("/add", (&configdict.ConfigDict{}).AddDict)
			dict.GET("/getDetail", (&configdict.ConfigDict{}).GetDetail)
			dict.POST("/update", (&configdict.ConfigDict{}).UpdateDict)
			dict.DELETE("/delete", (&configdict.ConfigDict{}).DeleteByDictId)
		}
		params := validRouter.Group("/config/params")
		{
			params.GET("/list", (&configparams.ConfigParams{}).GetParamsList)
			params.POST("/addConfig", (&configparams.ConfigParams{}).AddParams)
			params.POST("/updateConfig", (&configparams.ConfigParams{}).UpdateConfig)
			params.GET("/getParamsDetail", (&configparams.ConfigParams{}).GetConfigDetailById)
			params.DELETE("/delConfig", (&configparams.ConfigParams{}).DelConfig)
			params.DELETE("/getConfigByKey", (&configparams.ConfigParams{}).GetConfigByKey)
		}
		webSet := validRouter.Group("/config/webSet")
		{
			webSet.GET("/getInfo", (&configwebset.ConfigWebSet{}).GetInfo)
			webSet.POST("/update", (&configwebset.ConfigWebSet{}).UpdateInfo)
		}
		auths := validRouter.Group("/auth")
		{
			auths.GET("/menuList", (&authmenu.Auths{}).GetMenuListByParams)
			auths.GET("/getMenuDetailById", (&authmenu.Auths{}).GetMenuDetailById)
			auths.POST("/updateMenu", (&authmenu.Auths{}).UpdateMenu)
			auths.POST("/addMenu", (&authmenu.Auths{}).AddMenu)
			auths.GET("/treeSelect", (&authmenu.Auths{}).TreeSelect)
			auths.DELETE("/deleteMenu", (&authmenu.Auths{}).DeleteMenu)
			auths.GET("/roleList", (&roleParams.RoleParams{}).GetRoleList)
			auths.GET("/getMenuList", (&roleParams.RoleParams{}).GetMenuList)
			auths.POST("/addRole", (&roleParams.RoleParams{}).AddRole)
			auths.GET("/getRoleById", (&roleParams.RoleParams{}).GetRoleById)
			auths.POST("/updateRole", (&roleParams.RoleParams{}).UpdateRole)
			auths.DELETE("/deleteRole", (&roleParams.RoleParams{}).DeleteRole)
			// auths.GET("/userList", (&usermanagement.UserManagement{}).GetUserList)
		}
		dept := validRouter.Group("/dept")
		{
			dept.GET("/roleDeptTreeSelect", (&roleParams.RoleParams{}).RoleDeptTreeSelect)
			dept.GET("/listDept", (&department.Department{}).GetDeptList)
			dept.POST("/deptAdd", (&department.Department{}).AddDept)
			dept.POST("/updateDept", (&department.Department{}).UpdateDept)
			dept.GET("/getDeptById", (&department.Department{}).GetDeptById)
			dept.GET("/exclude", (&department.Department{}).ExcludeById)
			dept.DELETE("/deleteDept", (&department.Department{}).DeleteDept)
			dept.GET("/deptTreeSelect", (&department.Department{}).DeptTreeSelect)
		}
		roles := validRouter.Group("/role")
		{
			roles.POST("/roleDataScope", (&roleParams.RoleParams{}).RoleDataScope)
		}
		posts := validRouter.Group("/post")
		{
			posts.GET("/listPost", (&post.PostParams{}).ListPost)
			posts.POST("/addPost", (&post.PostParams{}).AddPost)
			posts.GET("/getPostById", (&post.PostParams{}).GetPostById)
			posts.POST("/updatePost", (&post.PostParams{}).UpdatePost)
			posts.DELETE("/deletePost", (&post.PostParams{}).DeletePost)
		}
		user := validRouter.Group("/user")
		{
			user.GET("/userList", (&usermanagement.UserManagement{}).GetUserList)
			user.GET("/getUser", (&usermanagement.UserManagement{}).GetUserInfo)
			user.POST("/addUser", (&usermanagement.UserManagement{}).AddUser)
			user.GET("/getUserInfoById", (&usermanagement.UserManagement{}).GetUserInfoById)
			user.POST("/updateUser", (&usermanagement.UserManagement{}).UpdateUser)
			user.DELETE("/delUser", (&usermanagement.UserManagement{}).DelUser)
			user.POST("/resetUserPwd", (&usermanagement.UserManagement{}).ResetUserPwd)
		}
		monitors := validRouter.Group("/monitor")
		{
			monitors.GET("/online/list", (&monitor.OnLine{}).GetOnlineList)
			monitors.GET("/job/getJobList", (&cron.CronJob{}).GetJobList)
			monitors.POST("/job/add", (&cron.CronJob{}).AddJob)
			monitors.GET("/job/getJobById", (&cron.CronJob{}).GetJobByList)
			monitors.POST("/job/updateJob", (&cron.CronJob{}).UpdateJob)
			monitors.DELETE("/job/delJobById", (&cron.CronJob{}).DelJobById)
			monitors.GET("/server/info", (&cron.CronJob{}).GetComputerInfo)
			monitors.GET("/loginLog/list", (&cron.CronJob{}).GetLoginLogList)
			monitors.DELETE("/loginLog/delLoginLog", (&cron.CronJob{}).DelLoginLog)
			monitors.GET("/operationLog/list", (&operationLog.OperationLog{}).GetOperationLogList)
			monitors.DELETE("/operationLog/delete", (&operationLog.OperationLog{}).DelOperationLog)
		}
		cms := validRouter.Group("/cms")
		{
			cms.GET("/menu/list", (&cmsmenuList.CmsMenuList{}).GetCmsMenuList)
			cms.GET("/menu/getTreeSelect", (&cmsmenuList.CmsMenuList{}).GetTreeSelect)
			cms.GET("/menu/modelOptions", (&cmsmenuList.CmsMenuList{}).GetModelOptions)
			cms.POST("/menu/addMenu", (&cmsmenuList.CmsMenuList{}).AddModelOptions)
			cms.GET("/menu/getMenuById", (&cmsmenuList.CmsMenuList{}).GetMenuById)
			cms.POST("/menu/updateCmsCategory", (&cmsmenuList.CmsMenuList{}).UpdateCmsCategory)
			cms.DELETE("/menu/deleteCategory", (&cmsmenuList.CmsMenuList{}).DeleteCategory)
			cms.DELETE("/news/listNews", (&cmsmenuList.CmsMenuList{}).GetListNews)
		}
	}
}
