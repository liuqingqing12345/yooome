package routers

import (
	"github.com/gin-gonic/gin"
	valid "yooome/routers/router"
)
// 总路由
func Routers(router *gin.Engine)  {
	r := router.Group("/captcha")
	valid.CaptchaRouters(r)
	r = router.Group("/admin")
	valid.AdminRouters(r)
	r = router.Group("/sysLogin")
	valid.SysLoginGroup(r)
	r = router.Group("/system")
	valid.SystemGroup(r)
}