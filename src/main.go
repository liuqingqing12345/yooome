package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"yooome/authorization"
	_ "yooome/bootstracp"
	"yooome/cors"
	_ "yooome/global/variable"
	"yooome/routers"
)

func main() {
	fmt.Println("yoooMe科技")
	gin.SetMode(gin.ReleaseMode)
	router := gin.Default()
	router.Use(cors.Cors())
	router.Use(authorization.CheckTokenAuth())
	routers.Routers(router)
	_ = router.Run(":80")
	fmt.Println("佑米科技结束运行")
}
