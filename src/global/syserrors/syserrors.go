package syserrors

const (
	ErrorsBasePath            string = "初始化项目根目录失败"
	ErrorsConfigInitFail      string = "初始化配置文件发生错误"
	ErrorsDialectorDbInitFail string = "gorm dialector 初始化失败,dbType:"
	// 数据库部分
	ErrorsDbDriverNotExists string = "数据库驱动类型不存在,目前支持的数据库类型：mysql、sqlserver、postgresql，您提交数据库类型："

	// CasBin 初始化可能的错误
	ErrorCasBinCanNotUseDbPtr   string = "CasBin 的初始化基于gorm 初始化后的数据库连接指针，程序检测到 gorm 连接指针无效，请检查数据库配置！"
	ErrorCasBinCreateAdaptFail  string = "CasBin NewAdapterByDBUseTableName 发生错误："
	ErrorCasBinNotFindModelFile string = "CasBin Model File Can Not Find: "
	ErrorCasBinEnforcer         string = "CasBin Enforcer Not Contribute: "
)
