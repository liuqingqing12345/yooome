package contsts
// 这里定义的常量，一般是具有错误代码 + 错误说明的，一般用于接口说明
const (
	// 表单验证器前缀
	ValidatorPrefix              string = "Form_Validator_"
	SequenceBits   = uint(12)             //序列所占的位数
	SequenceMask   = int64(-1 ^ (-1 << SequenceBits)) //
	StartTimeStamp = int64(1483228800000) //开始时间截 (2017-01-01)
	MachineIdBits  = uint(10)             //机器id所占的位数

	MachineIdShift = SequenceBits                     //机器id左移位数
	TimestampShift = SequenceBits + MachineIdBits     //时间戳左移位数


)