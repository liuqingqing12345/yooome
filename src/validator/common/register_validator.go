package common

import (
	"yooome/core/container"
	"yooome/global/contsts"
	"yooome/validator/users"
)

// 检查项目必须的非编译目录是否存在，避免在编译后调用的时候缺失相关目录

func RegisterValidator() {
	//创建一个容器
	containers := container.NewContainersFactory()
	//key 按照前缀 + 模块 +验证动作 格式，将各个模块验证器注册在容器中
	var key string
	// 在users模块中，模块表单验证器按照， key=>value形式注册在容器中，方便路由中调用
	key = contsts.ValidatorPrefix + "UserLogin"
	containers.Set(key, &users.Login{})
}
