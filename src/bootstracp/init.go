package bootstracp

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
	"yooome/global/variable"
	initcasbin "yooome/utils/casbin"
	"yooome/utils/gorminit"
	"yooome/utils/snow_flake"
	"yooome/utils/yml_config"
	"yooome/validator/common"
)

func init() {
	fmt.Println("init function ... ")
	// 2.检查配置文件以及日志目录等非编译性的必要条件
	checkRequiredFolders()
	// 3.初始化表参数单验证器，注册在容器中
	common.RegisterValidator()
	// 4.启动针对配置文件(config.yml, gorm2.yml)变化的监听，配置文件操作指针，初始化为全局变量
	variable.ConfigYml = yml_config.NewYamlFileFactory()
	variable.ConfigYml.ConfigFileChangeListen()
	// config>gorm_v2.yml 启动文件变化监听事件
	variable.ConfigGormYml = variable.ConfigYml.Clone("gorm")
	variable.ConfigGormYml.ConfigFileChangeListen()
	variable.NotCheckAuthAdminIds = variable.ConfigYml.GetIntSlice("adminInfo.notCheckAuthAdminIds")
	// 5.初始化全局日志句柄，并载入日志钩子函数
	// TODO
	// 6.初始化mysql数据库
	if variable.ConfigGormYml.GetInt("GormMysql.Mysql.IsInitGlobalGormMysql") == 1 {
		if dbMysql, err := gorminit.InitMysqlConnect(); err != nil {
			fmt.Println(" gormInit dbMysql ....", dbMysql)
		} else {
			variable.GormDBMysql = dbMysql
		}
	}
	fmt.Println(" 初始化mysql数据库 ")
	// 7.雪花算法全局变量
	variable.SnowFlake = snow_flake.NewSnowFlakeFactory()

	// 8.CasBin 依据配置文件设置参数（IsInit = 1)初始化
	if variable.ConfigYml.GetInt64("CasBin.IsInit") == 1 {
		var err error
		if variable.Enforcer, err = initcasbin.InitCasBinEnforcer(); err != nil {
			// 退出当前的系统
			logrus.Fatal(err.Error())
		}
	}
	fmt.Println("..................................")

}
func checkRequiredFolders() {
	// 1.检查配置文件是否存在
	if _, err := os.Stat(variable.BasePath + "/config/gorm.yml"); err != nil {
		fmt.Println("配置文件gorm.yml不存在....")
	}
	if _, err := os.Stat(variable.BasePath + "/public/"); err != nil {
		fmt.Println("public 目录不存在....")
	}
}
