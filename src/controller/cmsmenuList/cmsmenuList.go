package cmsmenuList

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"yooome/model/cmsmenu_params"
	"yooome/service/auth_service"
	"yooome/service/cmsmenu_service"
	"yooome/service/dept_service"
	"yooome/service/dict_service"
	"yooome/service/userinfo_service"
	"yooome/utils/convert"
	"yooome/utils/slicetree"
)

type CmsMenuList struct {
}

func (c *CmsMenuList) GetCmsMenuList(context *gin.Context) {
	var params cmsmenu_params.CmsMenuParams
	err := context.ShouldBind(&params)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	list, err := cmsmenu_service.GetCmsMenuList(&params)
	if err != nil {
		isSuccess = false
	}
	statusOptions, err := dict_service.GetStatusByDictType("sys_show_hide", "", "")
	if err != nil {
		isSuccess = false
	}
	// 栏目烈性
	typeOptions, err := dict_service.GetStatusByDictType("cms_category_type", "", "")
	data := map[string]interface{}{
		"list":          list,
		"statusOptions": statusOptions,
		"typeOptions":   typeOptions,
		"isSuccess":     isSuccess,
	}
	context.JSON(http.StatusOK, gin.H{
		"data": data,
	})
}

// 获取上级分类
func (c *CmsMenuList) GetTreeSelect(context *gin.Context) {
	// 获取上级分类
	menus, err := cmsmenu_service.GetTreeSelect()
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	// 获取分类栏目列表
	listTemp, contentTemp := cmsmenu_service.GetCmsTemplate()
	data := map[string]interface{}{
		"parentList":  menus,
		"listTemp":    listTemp,
		"contentTemp": contentTemp,
		"isSuccess":   isSuccess,
	}
	context.JSON(http.StatusOK, gin.H{
		"data": data,
	})
}

// 栏目模型选项
func (c *CmsMenuList) GetModelOptions(context *gin.Context) {
	modelOptions, err := dict_service.GetStatusByDictType("cms_cate_models", "", "")
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	values := convert.SliceAny(modelOptions["values"])
	// values := convert.SliceAny(modelValues)
	keys := make([]int64, len(values))
	for k, val := range values {
		data := convert.Map(val)
		keys[k] = convert.Int64(data["key"])
	}
	// 获取对应的模型
	model, err := cmsmenu_service.GetModelByCateIds(keys)
	context.JSON(http.StatusOK, gin.H{
		"data":      model,
		"isSuccess": isSuccess,
	})
}
func (c *CmsMenuList) AddModelOptions(context *gin.Context) {
	var params cmsmenu_params.AddMenuParams
	err := context.ShouldBindJSON(&params)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	isSuccess = cmsmenu_service.AddModelOptions(&params)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}
func (c *CmsMenuList) GetMenuById(context *gin.Context) {
	var id string
	id = context.Query("id")
	menuInfo, err := cmsmenu_service.GetMenuById(id)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	// 获取上级分类(频道）
	menus, err := cmsmenu_service.GetMenuListChannel()
	if err != nil {
		isSuccess = false
	}
	// 获取分类栏模板
	listTemp, contentTemp := cmsmenu_service.GetCmsTemplate()
	data := map[string]interface{}{
		"menuInfo":    menuInfo,
		"parentList":  menus,
		"listTemp":    listTemp,
		"contentTemp": contentTemp,
		"isSuccess":   isSuccess,
	}
	context.JSON(http.StatusOK, gin.H{
		"data": data,
	})

}
func (c *CmsMenuList) UpdateCmsCategory(context *gin.Context) {
	var params cmsmenu_params.UpdateMenuParams
	err := context.ShouldBindJSON(&params)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	isSuccess = cmsmenu_service.UpdateCmsCategory(&params)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}
func (c *CmsMenuList) DeleteCategory(context *gin.Context) {
	var ids []int64
	err := context.ShouldBindJSON(&ids)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	// 获取所有的栏目
	menus, err := cmsmenu_service.GetMenuListChannel()
	menuList := make([]map[string]interface{}, len(menus))
	for k, menu := range menus {
		menuList[k] = convert.Map(menu)
	}
	for _, id := range ids {
		children := slicetree.FindSonByParentId(menuList, id, "parentid", "id")
		for _, cid := range children {
			ids = append(ids, convert.Int64(cid["id"]))
		}
	}
	isSuccess = cmsmenu_service.DeleteCategory(ids)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}
func (c *CmsMenuList) GetListNews(context *gin.Context) {
	var params cmsmenu_params.QueryNewsParams
	err := context.ShouldBind(&params)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	// 获取登录用户 id
	userModel, err := userinfo_service.GetCurrentUser(context)
	if err != nil {
		isSuccess = false
	}
	// 获取当前用户的所属角色
	allRoles, err := auth_service.GetRoleList()
	if err != nil {
		isSuccess = false
	}
	roles, err := userinfo_service.GetAdminRole(userModel.Id, allRoles)
	if err != nil {
		isSuccess = false
	}
	// 获取角色对应的权限
	deptIdArr := make([]interface{}, 0, 100)
	for _, role := range roles {
		switch role.DataScope {
		case 1: // 全部数据
			return
		case 2: // 自定义数据权限
			var deptIds []int
			deptIds, err = dept_service.GetRoleDeptList(string(role.Id))
			if err != nil {
				return
			}
			deptIdArr = append(deptIdArr, deptIds)
		case 3: // 本部门数据权限
			deptIdArr = append(deptIdArr, userModel.DeptId)
		case 4: // 本部门数据及以下数据权限
			deptIdArr = append(deptIdArr, userModel.DeptId)
			// 获取正常状态部门数据
			depts, err := dept_service.GetList()
			if err != nil {
				isSuccess = false
			}
			var dList []map[string]interface{}
			for _, entity := range depts {
				m := map[string]interface{}{
					"id":    entity.DeptID,
					"pid":   entity.ParentID,
					"label": entity.DeptName,
				}
				dList = append(dList, m)
			}
			ls := slicetree.FindSonByParentId(dList, int64(userModel.DeptId), "pid", "id")
			for _, l := range ls {
				deptIdArr = append(deptIdArr, l["id"])
			}
		}
	}
	data := map[string]interface{}{
		"user.dept_id": deptIdArr,
	}
	total, page, list, err := cmsmenu_service.GetListNews(&params, data)
	if err != nil {
		isSuccess = false
	}
	// 获取可选栏目
	var cateId []int64
	for _, v := range params.CateId {
		cateId = append(cateId, int64(v))
	}
	menus, err := cmsmenu_service.GetModelByCateIds(cateId)
	if err != nil {
		isSuccess = false
	}
	// 发布文章状态
	statusOptions, err := dict_service.GetStatusByDictType("cms_news_pub_type", "", "")
	if err != nil {
		isSuccess = false
	}
	attrOptions, err := dict_service.GetStatusByDictType("cms_news_attr", "", "")
	if err != nil {
		isSuccess = false
	}
	typeOptions, err := dict_service.GetStatusByDictType("cms_news_type", "", "")
	if err != nil {
		isSuccess = false
	}
	result := map[string]interface{}{
		"currentPage":   page,
		"total":         total,
		"list":          list,
		"menus":         menus,
		"statusOptions": statusOptions,
		"attrOptions":   attrOptions,
		"typeOptions":   typeOptions,
		"isSuccess":     isSuccess,
	}
	context.JSON(http.StatusOK, gin.H{
		"data": result,
	})
}
