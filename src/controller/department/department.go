package department

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"yooome/model/sys_dept_params"
	"yooome/service/dept_service"
	"yooome/service/userinfo_service"
	"yooome/utils/slicetree"
)

type Department struct {
}

// 查询 部门tree
func (d *Department) RoleDeptTreeSelect(context *gin.Context) {
	roleId := context.Query("roleId")
	isSuccess := true
	if roleId != "" {
		isSuccess = false
	}
	list, err := dept_service.GetRoleDeptList(roleId)
	if err != nil {
		isSuccess = false
	}
	fmt.Println(isSuccess, list)
}

// 查询部门列表
func (d *Department) GetDeptList(context *gin.Context) {
	var deptParams sys_dept_params.DeptParams
	err := context.ShouldBind(&deptParams)
	isSuccess := true
	if err != nil && &deptParams != nil {
		isSuccess = false
	}
	list, err := dept_service.GetDeptList(&deptParams)

	context.JSON(http.StatusOK, gin.H{
		"data":      list,
		"isSuccess": isSuccess,
	})
}

// 添加部门
func (d *Department) AddDept(context *gin.Context) {
	var addDeptParams sys_dept_params.AddDeptParams
	err := context.ShouldBindJSON(&addDeptParams)
	isSuccess := true
	if err != nil && &addDeptParams != nil {
		isSuccess = false
	}
	user := userinfo_service.GetLoginAdminInfo(context)
	addDeptParams.CreateBy = user.UserName
	isSuccess = dept_service.AddDept(&addDeptParams)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}

// 修改部门信息
func (d *Department) UpdateDept(context *gin.Context) {
	var updateDeptParams sys_dept_params.UpdateDeptParams
	err := context.ShouldBindJSON(&updateDeptParams)
	isSuccess := true
	if err != nil && &updateDeptParams != nil {
		isSuccess = false
	}
	user := userinfo_service.GetLoginAdminInfo(context)
	updateDeptParams.UpdateBy = user.UserName
	isSuccess = dept_service.UpdateDept(&updateDeptParams)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}

// 更具id查询部门信息
func (d *Department) GetDeptById(context *gin.Context) {
	var id string
	id = context.Query("id")
	isSuccess := true
	if id == "" {
		isSuccess = false
	}
	dept := dept_service.GetDeptById(id)
	if dept == nil {
		isSuccess = false
	}
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
		"data":      dept,
	})
}

// 更具id查询部门信息
func (d *Department) ExcludeById(context *gin.Context) {
	var id string
	id = context.Query("id")
	isSuccess := true
	if id == "" {
		isSuccess = false
	}
	dept := dept_service.ExcludeById(id)
	if dept == nil {
		isSuccess = false
	}
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
		"data":      dept,
	})
}
func (d *Department) DeleteDept(context *gin.Context) {
	var id string
	id = context.Query("id")
	isSuccess := true
	if id == "" {
		isSuccess = false
	}
	isSuccess = dept_service.DeleteDept(id)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}
func (d *Department) DeptTreeSelect(context *gin.Context) {

	// 获取正常状态部门数据
	list, err := dept_service.GetList()
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	roleMap := make([]map[string]interface{}, len(list))
	for _, value := range list {
		data := map[string]interface{}{
			"id":    value.DeptID,
			"pid":   value.ParentID,
			"label": value.DeptName,
		}
		roleMap = append(roleMap, data)
	}
	deptList := slicetree.PushSonToParent(roleMap, 0, "pid", "id", "children", "", nil, false)

	res := map[string]interface{}{
		"depts": deptList,
	}
	context.JSON(http.StatusOK, gin.H{
		"data":      res,
		"isSuccess": isSuccess,
	})
}
