package monitor

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"yooome/model/monitor_params"
	"yooome/service/online_service"
)

type OnLine struct {
}

// 查询在线用户量列表
func (o *OnLine) GetOnlineList(context *gin.Context) {
	var params monitor_params.OnLineParams
	err := context.ShouldBind(&params)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	total, page, list, err := online_service.GetOnlineList(&params)
	if err != nil {
		isSuccess = false
	}
	fmt.Println(isSuccess)
	data := map[string]interface{}{
		"currentPage": page,
		"total":       total,
		"list":        list,
		"isSuccess":   isSuccess,
	}
	context.JSON(http.StatusOK, gin.H{
		"data": data,
	})
}
