package configparams

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/http"
	"yooome/model/sys_config_params"
	"yooome/service/dict_service"
	"yooome/service/params_service"
	"yooome/service/userinfo_service"
	"yooome/utils/convert"
)

type ConfigParams struct {
}

// 获取参数列表
func (p *ConfigParams) GetParamsList(context *gin.Context) {
	var params sys_config_params.ParamsPageReq
	// 获取参数
	err := context.ShouldBind(&params)
	if err != nil {
		logrus.Error("should bind params error", err)
	}
	// 获取分页参数
	total, page, list, err := params_service.GetParamsList(&params)
	// 系统内置选项
	paramsOption, err := dict_service.GetStatusByDictType("sys_yes_no", "", "")
	if err != nil {
		logrus.Error("paramsOption error ", err)
		return
	}
	result := map[string]interface{}{
		"currentPage": page,
		"total":       total,
		"list":        list,
		"searchTypes": paramsOption,
	}
	context.JSON(http.StatusOK, gin.H{
		"data": result,
	})
}

// 新增参数配置数据
func (c *ConfigParams) AddParams(context *gin.Context) {
	// 获取前台的数据构建结构体
	var params *sys_config_params.AddParamsReq
	var isSuccess int64
	err := context.ShouldBind(&params)
	if err != nil {
		isSuccess = 0
	}
	if params == nil {
		isSuccess = 0
	}
	// 检查 ConfigKey 是否为唯一
	isTrue := params_service.CheckConfigKeyUnique(params.ConfigKey)
	if !isTrue {
		logrus.Error("check config key is not unique ", err)
		return
	}
	// 获取登录用户的Id
	userId := userinfo_service.GetLoginID(context)
	// 存储参数信息
	isSuccess = params_service.AddConfigParams(params, userId)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}

// 修改参数列表
func (c *ConfigParams) UpdateConfig(context *gin.Context) {
	// 获取前台传递需要修改的参数
	var params *sys_config_params.UpdParamsReq
	var isSuccess int64
	err := context.ShouldBind(&params)
	if err != nil {
		isSuccess = 0
	}
	if params == nil {
		isSuccess = 0
	}
	// 获取当前登录用户的Id
	userId := userinfo_service.GetLoginID(context)
	// 更新当前的参数信息
	isSuccess = params_service.UpdateConfig(params, userId)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}

// 获取参数的详细信息
func (c *ConfigParams) GetConfigDetailById(context *gin.Context) {

	id := context.Query("id")
	var isSuccess bool
	if id != "" {
		isSuccess = false
	}
	// 获取参数信息
	param := params_service.GetConfigDetailById(convert.Uint64(id))
	if param != nil {
		isSuccess = true
	}
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
		"data":      param,
	})
}

// 删除参数数据
func (c *ConfigParams) DelConfig(context *gin.Context) {
	var configIds []int
	err := context.ShouldBindJSON(&configIds)
	if configIds == nil || err != nil {
		logrus.Error("context should bind dictId error ")
		return
	}
	isSuccess := params_service.DelConfig(configIds)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}

// 根据 ConfigKey 进行导出数据
func (c *ConfigParams) GetConfigByKey(context *gin.Context) {

}
