package login_log

type LoginLog struct {
	InfoId     int64  // 访问id
	LoginName  string // 登录账号
	Ipaddr     string // 登录地址
	LoginLocal string //登录地点
	Browser    string // 登录浏览器
	Os         string // 操作系统
	Status     int    // 登录状态(0成功，1失败）
	Msg        string // 提示信息
	LoginTime  int64  // 登录时间
	Module     string // 登录模块
}
