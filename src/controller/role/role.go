package roleParams

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"yooome/model/req_role_params"
	"yooome/model/role_params"
	"yooome/service/auth_service"
	"yooome/service/authrule_service"
	"yooome/service/dept_service"
	"yooome/service/dict_service"
	"yooome/utils/slicetree"
)

type RoleParams struct {
}

func (r *RoleParams) GetRoleList(context *gin.Context) {
	var roleParams req_role_params.ReqRoleParams
	err := context.ShouldBind(&roleParams)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	// 获取角色列表
	total, page, list, err := auth_service.GetRoleListByParams(&roleParams)
	if err != nil {
		isSuccess = false
	}
	// 菜单正常 or 停用
	statusOptions, err := dict_service.GetStatusByDictType("sys_normal_disable", "", "")
	if err != nil {
		isSuccess = false
	}
	result := map[string]interface{}{
		"currentPage": page,
		"total":       total,
		"list":        list,
		"searchTypes": statusOptions,
		"isSuccess":   isSuccess,
	}
	context.JSON(http.StatusOK, gin.H{
		"data": result,
	})
}

// 获取 菜单 tree
func (r *RoleParams) GetMenuList(context *gin.Context) {
	// 获取父级菜单信息
	menuList, err := authrule_service.GetMenuList()
	isSuccess := true
	if err != nil {
		isSuccess = false
	}

	var menusMap = make([]map[string]interface{}, len(menuList))
	for _, v := range menuList {
		m := map[string]interface{}{
			"id":    v.Id,
			"pid":   v.Pid,
			"label": v.Title,
		}
		menusMap = append(menusMap, m)
	}
	menuSort := slicetree.PushSonToParent(menusMap)
	data := map[string]interface{}{
		"isSuccess": isSuccess,
		"menuList":  menuSort,
	}
	context.JSON(http.StatusOK, gin.H{
		"data": data,
	})
}

// 新增角色
func (r *RoleParams) AddRole(context *gin.Context) {
	var roleParams req_role_params.AddRoleParams
	err := context.ShouldBind(&roleParams)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	isSuccess = auth_service.AddRole(&roleParams)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}

// 根据 id 获取 role
func (r *RoleParams) GetRoleById(context *gin.Context) {
	roleId := context.Query("roleId")
	isSuccess := true
	if roleId == "" {
		isSuccess = false
	}
	role := auth_service.GetRoleById(roleId)
	data := map[string]interface{}{
		"role": role,
	}
	context.JSON(http.StatusOK, gin.H{
		"data":      data,
		"isSuccess": isSuccess,
	})
}

// 更新角色信息
func (r *RoleParams) UpdateRole(ctx *gin.Context) {
	var roleParams req_role_params.UpdateRoleParams
	err := ctx.ShouldBind(&roleParams)
	isSuss := true
	if err != nil && &roleParams != nil {
		isSuss = false
	}
	isSuss = auth_service.UpdateRole(&roleParams)
	ctx.JSON(http.StatusOK, gin.H{
		"isSucc": isSuss,
	})
}

// 删除角色信息
func (r *RoleParams) DeleteRole(context *gin.Context) {
	var roleIds []int
	err := context.ShouldBindJSON(&roleIds)
	isSuccess := true
	if err != nil && len(roleIds) > 0 {
		isSuccess = false
	}
	isSuccess = auth_service.DeleteRole(roleIds)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}
func (r *RoleParams) RoleDeptTreeSelect(context *gin.Context) {
	var roleId string
	roleId = context.Query("roleId")
	isSuccess := true
	if roleId == "" {
		isSuccess = false
	}
	// 获取撑场状态部门数据
	list, err := dept_service.GetList()
	if err != nil {
		isSuccess = false
	}
	roleMap := make([]map[string]interface{}, len(list))
	for _, value := range list {
		data := map[string]interface{}{
			"id":    value.DeptID,
			"pid":   value.ParentID,
			"label": value.DeptName,
		}
		roleMap = append(roleMap, data)
	}
	// 获取关联的角色数据权限
	checkedKeys, err := dept_service.GetRoleDeptList(roleId)
	if err != nil {
		isSuccess = false
	}
	deptList := slicetree.PushSonToParent(roleMap)

	res := map[string]interface{}{
		"depts":       deptList,
		"checkedKeys": checkedKeys,
	}
	context.JSON(http.StatusOK, gin.H{
		"data":      res,
		"isSuccess": isSuccess,
	})
}

// 修改数据权限
func (r *RoleParams) RoleDataScope(context *gin.Context) {
	var roleParams role_params.RoleParams
	err := context.ShouldBindJSON(&roleParams)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	isSuccess = auth_service.RoleDataScope(&roleParams)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}
