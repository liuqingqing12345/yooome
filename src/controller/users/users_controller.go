package userscontroller

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/http"
	"yooome/global/contsts"
	"yooome/global/variable"
	model "yooome/model/users"
	"yooome/token"
	"yooome/utils/md5_encrypt"
	"yooome/utils/tools"
)

type Users struct{}

func (u *Users) Login(context *gin.Context) {
	userName := context.GetString(contsts.ValidatorPrefix + "username")
	pass := context.GetString(contsts.ValidatorPrefix + "password")
	phone := context.GetString(contsts.ValidatorPrefix + "phone")
	userModel := model.NewUserFactory("").Login(userName, pass)

	if userModel == nil {
		logrus.Error("user model login error ", userModel)
		return
	}
	publicKey := variable.ConfigYml.GetString("adminInfo.encryptKey")
	// 用户密码进行加密
	password := tools.EncryptCBC(pass, publicKey)
	var userKey string
	userKey = userName + password + md5_encrypt.MustEncryptString(context.ClientIP())
	fmt.Println("must encrypt string userKey = ", userKey, "context client ip = ", context.ClientIP())
	fmt.Println("用户phone = ", phone, "用户密码 = ", password)
	// 获取token
	getToken := token.NewMToken()
	result := getToken.GenToken(userKey, userModel)
	fmt.Println("GenToken result = ", result)
	// 设置用户的token
	//返回数据
	if result.Data != "" {
		context.JSON(http.StatusOK, gin.H{
			"data": result,
		})
	}
}
