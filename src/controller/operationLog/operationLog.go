package operationLog

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"yooome/model/operation_params"
	"yooome/service/dict_service"
	"yooome/service/operation_service"
)

type OperationLog struct {
}

func (o *OperationLog) GetOperationLogList(context *gin.Context) {
	// 定义接受前端的参数结构体
	var params operation_params.OperationParams
	isSuccess := true
	err := context.ShouldBind(&params)
	if err != nil && &params != nil {
		isSuccess = false
	}
	total, page, list, err := operation_service.GetOperationLogList(&params)
	// 获取相关选项
	status, err := dict_service.GetStatusByDictType("sys_oper_log_status", "", "全部")
	if err != nil {
		isSuccess = false
	}
	data := map[string]interface{}{
		"currentPage":  page,
		"total":        total,
		"list":         list,
		"searchStatus": status,
		"isSuccess":    isSuccess,
	}
	context.JSON(http.StatusOK, gin.H{
		"data": data,
	})
}

// 删除按钮
func (o *OperationLog) DelOperationLog(context *gin.Context) {
	var ids []int
	err := context.ShouldBindJSON(&ids)
	isSuccess := true
	if err != nil && len(ids) > 0 {
		isSuccess = false
	}
	isSuccess = operation_service.DelOperationLog(ids)
	context.JSON(http.StatusOK, gin.H{
		"data": isSuccess,
	})
}
