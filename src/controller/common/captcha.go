package common

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
	"yooome/service"
)

type Captcha struct{}
type UserLogin struct {
	Id       string `form:"id" json:"id"`
	Bas64    string `form:"bas64" json:"bas64"`
	UserName string `form:"username" json:"username"`
	PassWord string `form:"password" json:"password"`
}

//
func (c *Captcha) GetImg(context *gin.Context) {
	id, bas64 := service.GetCaptchaDriverString()
	context.JSON(http.StatusOK, gin.H{
		"msg":   "ok",
		"id":    id,
		"bas64": bas64,
	})
	fmt.Println("id = ", id)
	fmt.Println("bas64 = ", bas64)
	fmt.Println("获取图片验证码")
}
func (c *Captcha) ValidCaptcha(context *gin.Context) {

	// var user UserLogin,
	// err := context.ShouldBind(&user)
	// data := context.PostFormMap("Data")
	//id := context.PostForm("id")
	//bas64 := context.PostForm("bas64")
	var loginMsg map[string]string
	data, _ := ioutil.ReadAll(context.Request.Body)
	err := json.Unmarshal(data, &loginMsg)
	if err != nil {
		fmt.Println("json unmarshal err ", err)
	}
	fmt.Printf("context.Request.body: %v", string(data))
	id := loginMsg["id"]
	bas64 := loginMsg["bas64"]
	fmt.Println("id=", id)
	fmt.Println("bas64=", bas64)
	boo := service.VerifyCaptchaString(id, bas64)
	fmt.Println("validate context captcha boo = ", boo)
	context.JSON(http.StatusOK, gin.H{
		"msg": "ok",
	})
}
