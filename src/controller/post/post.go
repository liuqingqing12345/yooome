package post

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
	"yooome/model/post_params"
	"yooome/service/post_service"
	"yooome/service/userinfo_service"
)

type PostParams struct {
}

func (p *PostParams) ListPost(context *gin.Context) {
	var params post_params.SearchPostParams
	isSuccess := true
	err := context.ShouldBindJSON(&params)
	if err != nil {
		isSuccess = false
	}
	total, page, list, err := post_service.ListPost(&params)
	data := map[string]interface{}{
		"total":     total,
		"list":      list,
		"page":      page,
		"isSuccess": isSuccess,
	}
	context.JSON(http.StatusOK, gin.H{
		"data": data,
	})
}
func (p *PostParams) AddPost(context *gin.Context) {
	var params post_params.AddPostParams
	err := context.ShouldBindJSON(&params)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	user := userinfo_service.GetLoginAdminInfo(context)
	params.CreateBy = user.UserName
	params.CreateTime = time.Now()
	isSuccess = post_service.AddPost(&params)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}
func (p *PostParams) GetPostById(context *gin.Context) {
	var id string
	id = context.Query("id")
	res, err := post_service.GetPostById(id)
	isSuccess := true
	if err != nil && res != nil {
		isSuccess = false
	}

	context.JSON(http.StatusOK, gin.H{
		"data":      res,
		"isSuccess": isSuccess,
	})
}

// 修改岗位
func (p *PostParams) UpdatePost(context *gin.Context) {
	var param post_params.UpdatePostParams
	err := context.ShouldBindJSON(&param)
	user := userinfo_service.GetLoginAdminInfo(context)
	param.UpdateBy = user.UserName
	param.UpdateTime = time.Now()
	isSuccess := true
	if err != nil && &param != nil {
		isSuccess = false
	}
	isSuccess = post_service.UpdatePost(&param)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}
func (p *PostParams) DeletePost(context *gin.Context) {
	var postIds []int
	err := context.ShouldBindJSON(&postIds)
	isSuccess := true
	if err != nil && &postIds != nil {
		isSuccess = false
	}
	isSuccess = post_service.DeletePost(postIds)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}
