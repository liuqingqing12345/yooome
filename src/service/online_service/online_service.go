package online_service

import (
	"yooome/model/monitor_params"
	"yooome/model/online_model"
)

// 获取登录用户的信息
func GetOnlineList(params *monitor_params.OnLineParams) (total, page int, online []*online_model.OnlineModel, err error) {
	return online_model.NewOnlineModelFactory("").GetOnlineList(params)
}
