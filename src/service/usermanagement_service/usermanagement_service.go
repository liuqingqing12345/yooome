package usermanagement_service

import (
	"yooome/model/user_params"
	model "yooome/model/users"
)

func GetUserList(userParams *user_params.GetUserParams) (total, page int, list []*model.UserModel, err error) {
	return model.NewUserFactory("").GetUserList(userParams)
}
