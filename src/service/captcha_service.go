package service

import (
	"github.com/mojocn/base64Captcha"
	log "github.com/sirupsen/logrus"
	"strings"
)

// 获取字母数字混合验证码
func GetCaptchaDriverString() (id string, bas64 string) {
	driver := &base64Captcha.DriverString{
		Height:          80,
		Width:           240,
		NoiseCount:      50,
		ShowLineOptions: 20,
		Length:          4,
		Source:          "1234567890qwertyuioplkjhgfdsazxcvbnm",
		Fonts:           []string{"RitaSmith.ttf"},
	}
	driver = driver.ConvertFonts()
	store := base64Captcha.DefaultMemStore
	captcha := base64Captcha.NewCaptcha(driver, store)
	id, bas64, err := captcha.Generate()
	if err != nil {
		log.Error("获取验证图片error：", err)
	}
	return
}

// 验证输入的验证码是否正确
func VerifyCaptchaString(id string, answer string) bool {
	driver := new(base64Captcha.DriverString)
	store := base64Captcha.DefaultMemStore
	captcha := base64Captcha.NewCaptcha(driver, store)
	answer = strings.ToLower(answer)
	return captcha.Verify(id, answer, true)
}
