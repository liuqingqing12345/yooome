package loginlog_service

import (
	"yooome/model/loginlog_model"
	"yooome/model/loginlog_params"
)

func GetLoginLogList(params *loginlog_params.GetLoginLogParams) (total, page int, list []*loginlog_model.LoginLogModel, err error) {
	return loginlog_model.NewLoginLogFactory("").GetLoginLogList(params)
}
func DelLoginLog(ids []int) bool {
	return loginlog_model.NewLoginLogFactory("").DelLoginLog(ids)
}
