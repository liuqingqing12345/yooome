package auth_service

import (
	"yooome/model/req_role_params"
	"yooome/model/role"
	"yooome/model/role_params"
)

func GetRoleList() (list []*role.Role, err error) {
	// TODO 从缓存中查询 role
	// 数据库中查询 role 角色信息
	return role.NewRoleFactory("").GetRoleList()
	// TODO 将查询出来的 role 角色信息进行缓存
}

func GetRoleListByParams(roleParams *req_role_params.ReqRoleParams) (total, page int, list []*role.Role, err error) {
	return role.NewRoleFactory("").GetMenuListByParams(roleParams)
}

// 添加角色
func AddRole(roleParams *req_role_params.AddRoleParams) bool {
	return role.NewRoleFactory("").AddRole(roleParams)
}
func GetRoleById(id string) (r *role.Role) {
	return role.NewRoleFactory("").GetRoleById(id)
}
func UpdateRole(roleParams *req_role_params.UpdateRoleParams) bool {
	return role.NewRoleFactory("").UpdateRole(roleParams)
}
func DeleteRole(ids []int) bool {
	return role.NewRoleFactory("").DeleteRole(ids)
}
func RoleDataScope(roleParams *role_params.RoleParams) bool {
	return role.NewRoleFactory("").RoleDataScope(roleParams)
}
func AddUserRole(roleIds []int, userId int64) bool {
	return role.NewRoleFactory("").AddUserRole(roleIds, userId)
}
func GetAdminRoleIds(id string) (roleIds []int64, err error) {
	return role.NewRoleFactory("").GetAdminRoleIds(id)
}
