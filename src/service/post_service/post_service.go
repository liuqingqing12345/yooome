package post_service

import (
	"yooome/model/post_model"
	"yooome/model/post_params"
)

func ListPost(params *post_params.SearchPostParams) (total, page int, list []*post_model.PostModel, err error) {
	return post_model.NewPostModelFactory("").ListPost(params)
}
func AddPost(params *post_params.AddPostParams) bool {
	return post_model.NewPostModelFactory("").AddPost(params)
}
func GetPostById(id string) (post *post_model.PostModel, err error) {
	return post_model.NewPostModelFactory("").GetPostById(id)
}
func UpdatePost(param *post_params.UpdatePostParams) bool {
	return post_model.NewPostModelFactory("").UpdatePost(param)
}
func DeletePost(ids []int) bool {
	return post_model.NewPostModelFactory("").DeletePost(ids)
}
func GetListPost() (list []*post_model.PostModel, err error) {
	return post_model.NewPostModelFactory("").GetListPost()
}
func GetStatusPost() (list []*post_model.PostModel, err error) {
	return post_model.NewPostModelFactory("").GetStatusPost()
}
func GetUserPostById(id string) (postIds []int64, err error) {
	return post_model.NewPostModelFactory("").GetUserPostById(id)
}
