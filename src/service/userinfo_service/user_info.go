package userinfo_service

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"strings"
	"yooome/global/variable"
	"yooome/model/auth_rule"
	"yooome/model/role"
	"yooome/model/user_params"
	model "yooome/model/users"
	"yooome/service/authrule_service"
	"yooome/token"
	"yooome/utils/convert"
	"yooome/utils/slicetree"
	"yooome/utils/utilsstr"
)

// 获取当前登录用户信息，直接从数据库中获取
func GetCurrentUser(context *gin.Context) (*model.UserModel, error) {
	id := GetLoginID(context)
	Authorization := context.Request.Header.Get("Authorization")
	fmt.Println("Authorization = ", Authorization)
	fmt.Print("id = ", id)
	// 获取当前登录用户信息
	userInfo, err := model.NewUserFactory("").GetUserById(id)
	return userInfo, err
}

// 获取用户登录id
func GetLoginID(context *gin.Context) (userId int64) {
	userInfo := GetLoginAdminInfo(context)
	if userInfo != nil {
		userId = userInfo.Id
	}
	// 获取用户角色信息
	//
	return userId
}

// 获取缓存的用户信息
func GetLoginAdminInfo(context *gin.Context) *model.UserModel {
	// 获取请求发送过来的token
	result := token.NewMToken().GetTokenData(context)
	userInfo := convert.Map(result.Data)
	user := userInfo["data"]
	var userModel *model.UserModel
	_ = json.Unmarshal([]byte(convert.String(user)), &userModel)
	fmt.Println("userModel.Id = ", userModel.Id)
	fmt.Println("userModel.UserPassword = ", userModel.UserPassword)
	fmt.Println("userModel.UserName = ", userModel.UserName)
	return userModel
}

// 获取管理员的角色信息
func GetAdminRole(userId int64, allRoleList []*role.Role) (roles []*role.Role, err error) {
	roleIds, err := GetAdminRoleIds(userId)
	if err != nil {
		return
	}
	roles = make([]*role.Role, 0, len(roleIds))
	for _, v := range allRoleList {
		for _, id := range roleIds {
			if id == v.Id {
				roles = append(roles, v)
			}
		}
		if len(roles) == len(roleIds) {
			break
		}
	}
	return
}

func GetAdminRoleIds(userId int64) (roleIds []int64, err error) {
	// 获取 CasBin Enforcer 执行器
	enforcer := variable.Enforcer
	// 查询关联角色规则
	groupPolicy := enforcer.GetFilteredGroupingPolicy(0, fmt.Sprintf("u_%d", userId))
	if len(groupPolicy) > 0 {
		roleIds = make([]int64, len(groupPolicy))
		// 得到切片的角色的id
		for k, v := range groupPolicy {
			policyId := strings.Split(v[1], "_")
			roleIds[k] = convert.Int64(policyId[1])
		}
	}
	return
}

// 获取 按钮是否开启
func GetPermissions(roleIds []int64) ([]string, error) {
	// 获取角色对应的菜单 id
	enforcer := variable.Enforcer
	if enforcer == nil {
		return nil, errors.New("enforcer can not find")
	}
	menuIds := map[int64]int64{}
	for _, roleId := range roleIds {
		// 查询当前的权限
		polices := enforcer.GetFilteredPolicy(0, fmt.Sprintf("g_%d", roleId))
		for _, policy := range polices {
			mid := convert.Int64(strings.Split(policy[1], "_"))
			menuIds[mid] = mid
		}
	}
	// 获取所有开启的按钮
	allButtons, err := authrule_service.GetIsButtonStatusList()
	if err != nil {
		return nil, errors.New("get button status error")
	}
	userButtons := make([]string, len(allButtons))
	for _, button := range allButtons {
		if _, ok := menuIds[convert.Int64(button.Id)]; strings.EqualFold(button.Condition, "nocheck") || ok {
			userButtons = append(userButtons, button.Name)
		}
	}
	return userButtons, nil
}

// 获取所有的菜单信息
func GetAllMenus() ([]map[string]interface{}, error) {
	//  获取所有开启的菜单
	allMenus, err := authrule_service.GetIsMenuStatusList()
	if err != nil {
		return nil, err
	}
	// 定义一个 map 存储 menus信息
	menus := make([]map[string]interface{}, len(allMenus))
	for k, v := range allMenus {
		menu := convert.Map(v)
		menu = setMenuMap(menu, v)
		menus[k] = menu
	}
	menus = slicetree.PushSonToParent(menus, 0, "pid", "id", "children", "", nil, true)
	return menus, nil
}

// 组合返回 menu 前端数据
func setMenuMap(menu map[string]interface{}, auth *auth_rule.AuthRule) map[string]interface{} {
	menu["index"] = auth.Name
	menu["name"] = utilsstr.UpFirst(auth.Path)
	menu["menuname"] = auth.Title
	if auth.MenuType != 0 {
		menu["component"] = auth.Component
		menu["path"] = auth.Path
	} else {
		menu["path"] = "/" + auth.Path
		menu["component"] = "Layout"
	}
	menu["meta"] = map[string]string{
		"icon":  auth.Icon,
		"title": auth.Title,
	}
	if auth.AlwaysShow == 1 {
		menu["hidden"] = false
	} else {
		menu["hidden"] = true
	}
	if auth.AlwaysShow == 1 && auth.MenuType == 0 {
		menu["alwaysshow"] = true
	} else {
		menu["alwaysshow"] = false
	}
	return menu
}

// 获取管理员所属角色菜单
func GetAdminMenusByRoleIds(roleIds []int64) (menus []map[string]interface{}, err error) {
	// 获取角色对应的菜单id
	enforcer := variable.Enforcer
	if enforcer == nil {
		return nil, errors.New("create variable enforcer error, please check")
	}
	menuIds := map[int64]int64{}
	for _, roleId := range roleIds {
		// 查询当前的权限
		polices := enforcer.GetFilteredPolicy(0, fmt.Sprintf("g_%d", roleId))
		for _, policy := range polices {
			result := strings.Split(policy[1], "_")
			mid := convert.Int64(result[1])
			menuIds[mid] = mid
		}
	}
	// 获取所有开启的菜单
	allMenus, err := authrule_service.GetIsMenuStatusList()
	if err != nil {
		return
	}
	roleMenus := make([]map[string]interface{}, len(allMenus))
	for _, v := range allMenus {
		if _, ok := menuIds[v.Id]; strings.EqualFold(v.Condition, "nocheck") || ok {
			roleMenu := convert.Map(v)
			roleMenu = setMenuMap(roleMenu, v)
			roleMenus = append(roleMenus, roleMenu)
		}
	}
	menus = slicetree.PushSonToParent(roleMenus, 0, "pid", "id", "children", "", nil, true)
	return menus, nil
}

// 获取登录用户的 ID
func GetLoginId(context *gin.Context) (userId int64) {
	userInfo := GetLoginAdminInfo(context)
	if userInfo != nil {
		userId = userInfo.Id
	}
	return
}

func AddUser(addUserParams *user_params.AddUserParams) int64 {
	return model.NewUserFactory("").AddUser(addUserParams)
}
func AddUserPost(roleIds []int, userId int64) bool {
	return model.NewUserFactory("").AddUserPost(roleIds, userId)
}
func GetUserInfoById(id string) (user *model.UserModel) {
	return model.NewUserFactory("").GetUserInfoById(id)
}

// 修改用户信息
func UpdateUserInfo(params *user_params.UpdateUserParams) bool {
	return model.NewUserFactory("").UpdateUserInfo(params)
}
func UpdateUserRole(ids []int, userId int) bool {
	return model.NewUserFactory("").UpdateUserRole(ids, userId)
}
func DelUser(userIds []int64) bool {
	return model.NewUserFactory("").DelUser(userIds)
}
func DelUserPost(userIds []int64) bool {
	return model.NewUserFactory("").DelUserPost(userIds)
}
func ResetUserPwd(params *user_params.ResetPwdRew) bool {
	return model.NewUserFactory("").ResetUserPwd(params)
}
