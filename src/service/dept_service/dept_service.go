package dept_service

import (
	"yooome/model/role_dept_model"
	"yooome/model/sys_dept_model"
	"yooome/model/sys_dept_params"
)

func GetRoleDeptList(roleId string) (list []int, err error) {
	return role_dept_model.NewRoleDeptModelFactory("").GetRoleDeptList(roleId)
}
func GetList() (list []*sys_dept_model.Dept, err error) {
	return sys_dept_model.NewDeptModelFactory("").GetList()
}
func GetDeptList(deptParams *sys_dept_params.DeptParams) (list []*sys_dept_model.Dept, err error) {
	return sys_dept_model.NewDeptModelFactory("").GetDeptList(deptParams)
}
func AddDept(addDeptParams *sys_dept_params.AddDeptParams) bool {
	return sys_dept_model.NewDeptModelFactory("").AddDept(addDeptParams)
}
func UpdateDept(update *sys_dept_params.UpdateDeptParams) bool {
	return sys_dept_model.NewDeptModelFactory("").UpdateDept(update)
}
func GetDeptById(id string) (dept *sys_dept_model.Dept) {
	return sys_dept_model.NewDeptModelFactory("").GetDeptById(id)
}
func ExcludeById(id string) (dept []*sys_dept_model.Dept) {
	return sys_dept_model.NewDeptModelFactory("").ExcludeById(id)
}
func DeleteDept(id string) bool {
	return sys_dept_model.NewDeptModelFactory("").DeleteDept(id)
}
